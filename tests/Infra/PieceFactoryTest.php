<?php

declare(strict_types=1);

namespace LVC\MinisTablesTests\Infra;

use LVC\MinisTables\Domain\GroundSection;
use LVC\MinisTables\Domain\Piece;
use LVC\MinisTables\Domain\Side;
use LVC\MinisTables\Infra\PieceFactory;
use PHPUnit\Framework\TestCase;

class PieceFactoryTest extends TestCase
{
    public function testThrowsWithoutAValidRepresentation(): void
    {
        self::expectException(\RuntimeException::class);
        self::expectExceptionMessage(
            'Invalid piece representation given (0:grass), it must matches "(?<topSide>\d+:(?:asphalt|concrete|cobbles|dirt|grass|path|rock|sand|stone|water)(?:;\d+:(?:asphalt|concrete|cobbles|dirt|grass|path|rock|sand|stone|water))*)\^(?<rightSide>\d+:(?:asphalt|concrete|cobbles|dirt|grass|path|rock|sand|stone|water)(?:;\d+:(?:asphalt|concrete|cobbles|dirt|grass|path|rock|sand|stone|water))*)\^(?<bottomSide>\d+:(?:asphalt|concrete|cobbles|dirt|grass|path|rock|sand|stone|water)(?:;\d+:(?:asphalt|concrete|cobbles|dirt|grass|path|rock|sand|stone|water))*)\^(?<leftSide>\d+:(?:asphalt|concrete|cobbles|dirt|grass|path|rock|sand|stone|water)(?:;\d+:(?:asphalt|concrete|cobbles|dirt|grass|path|rock|sand|stone|water))*)<(?<name>\w+)>".'
        );

        PieceFactory::createPiece('0:grass');
    }

    /**
     * @dataProvider getPieceTestCases
     */
    public function testCreatePiece(string $pieceRepresentation, Piece $expectedPiece): void
    {
        $piece = PieceFactory::createPiece($pieceRepresentation);

        self::assertInstanceOf(Piece::class, $piece);
        self::assertEqualsCanonicalizing($expectedPiece, $piece);
    }

    /**
     * @return \Generator<string, array{string, Piece}>
     */
    public function getPieceTestCases(): \Generator
    {
        yield 'Single section' => [
            '0:grass^0:grass^0:grass^0:grass<plain>',
            new Piece(
                'plain',
                new Side(
                    new GroundSection('grass', 0),
                ),
                new Side(
                    new GroundSection('grass', 0),
                ),
                new Side(
                    new GroundSection('grass', 0),
                ),
                new Side(
                    new GroundSection('grass', 0),
                ),
            ),
        ];

        yield 'Three sections' => [
            '0:grass;0:path;0:water^0:water;0:path;1:grass^1:grass;0:path;1:rock^1:rock;0:path;0:grass<crossroad>',
            new Piece(
                'crossroad',
                new Side(
                    new GroundSection('water', 0),
                    new GroundSection('path', 0),
                    new GroundSection('grass', 0),
                ),
                new Side(
                    new GroundSection('grass', 1),
                    new GroundSection('path', 0),
                    new GroundSection('water', 0),
                ),
                new Side(
                    new GroundSection('rock', 1),
                    new GroundSection('path', 0),
                    new GroundSection('grass', 1),
                ),
                new Side(
                    new GroundSection('grass', 0),
                    new GroundSection('path', 0),
                    new GroundSection('rock', 1),
                ),
            ),
        ];

        yield 'Different sections' => [
            '0:grass^0:grass;0:path;1:rock^1:rock;0:water^0:water;0:dirt;0:path;0:grass<highlands>',
            new Piece(
                'highlands',
                new Side(
                    new GroundSection('grass', 0),
                ),
                new Side(
                    new GroundSection('rock', 1),
                    new GroundSection('path', 0),
                    new GroundSection('grass', 0),
                ),
                new Side(
                    new GroundSection('water', 0),
                    new GroundSection('rock', 1),
                ),
                new Side(
                    new GroundSection('grass', 0),
                    new GroundSection('path', 0),
                    new GroundSection('dirt', 0),
                    new GroundSection('water', 0),
                ),
            ),
        ];
    }
}
