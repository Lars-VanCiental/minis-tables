<?php

declare(strict_types=1);

namespace LVC\MinisTablesTests\Infra;

use LVC\MinisTables\Infra\GroundTexture;
use LVC\MinisTables\Infra\PieceRepresentationParser;
use PHPUnit\Framework\TestCase;

class PieceRepresentationParserTest extends TestCase
{
    /**
     * @dataProvider getInvalidParsingTestCases
     */
    public function testInvalidParsing(
        string $pieceRepresentation,
    ): void {
        $parser = new PieceRepresentationParser($pieceRepresentation);
        self::assertSame($pieceRepresentation, $parser->pieceRepresentation);
        self::assertFalse($parser->isValid);
    }

    public function getInvalidParsingTestCases(): \Generator
    {
        yield 'Missing name' => [
            '0:grass^0:grass^0:grass^0:grass',
        ];

        yield 'Missing one side' => [
            '0:grass^0:grass^0:grass<test>',
        ];

        yield 'Missing one height' => [
            '0:grass^0:grass^grass^0:grass<plain>',
        ];

        yield 'Missing one texture' => [
            '0:grass^0:grass^0^0:grass<test>',
        ];

        yield 'Unknown texture' => [
            '0:grass^0:grass^0:something^0:grass<test>',
        ];

        yield 'Unknown height' => [
            '0:grass^0:grass^zero:grass^0:grass<test>',
        ];

        yield 'Wrong side separator' => [
            '0:grass|0:grass|0:grass|0:grass<test>',
        ];

        yield 'Missing section separator' => [
            '0:grass0:grass^0:grass^0:grass^0:grass<test>',
        ];

        yield 'Wrong section separator' => [
            '0:grass,0:grass^0:grass^0:grass^0:grass<test>',
        ];
    }

    /**
     * @dataProvider getValidParsingTestCases
     *
     * @param array<mixed> $expectedTopSideArray
     * @param array<mixed> $expectedRightSideArray
     * @param array<mixed> $expectedBottomSideArray
     * @param array<mixed> $expectedLeftSideArray
     */
    public function testValidParsing(
        string $pieceRepresentation,
        string $expectedName,
        array $expectedTopSideArray,
        array $expectedRightSideArray,
        array $expectedBottomSideArray,
        array $expectedLeftSideArray,
    ): void {
        $parser = new PieceRepresentationParser($pieceRepresentation);
        self::assertSame($pieceRepresentation, $parser->pieceRepresentation);
        self::assertTrue($parser->isValid);
        self::assertSame($expectedName, $parser->name);
        self::assertSame($expectedTopSideArray, $parser->topSide);
        self::assertSame($expectedRightSideArray, $parser->rightSide);
        self::assertSame($expectedBottomSideArray, $parser->bottomSide);
        self::assertSame($expectedLeftSideArray, $parser->leftSide);
    }

    public function getValidParsingTestCases(): \Generator
    {
        yield 'Single section' => [
            '0:grass^0:grass^0:grass^0:grass<plain>',
            'plain',
            [
                [
                    'height' => 0,
                    'texture' => GroundTexture::GRASS,
                ],
            ],
            [
                [
                    'height' => 0,
                    'texture' => GroundTexture::GRASS,
                ],
            ],
            [
                [
                    'height' => 0,
                    'texture' => GroundTexture::GRASS,
                ],
            ],
            [
                [
                    'height' => 0,
                    'texture' => GroundTexture::GRASS,
                ],
            ],
        ];

        yield 'Three sections' => [
            '0:grass;0:path;0:water^0:water;0:path;1:grass^1:grass;0:path;1:rock^1:rock;0:path;0:grass<crossroad>',
            'crossroad',
            [
                [
                    'height' => 0,
                    'texture' => GroundTexture::WATER,
                ],
                [
                    'height' => 0,
                    'texture' => GroundTexture::PATH,
                ],
                [
                    'height' => 0,
                    'texture' => GroundTexture::GRASS,
                ],
            ],
            [
                [
                    'height' => 1,
                    'texture' => GroundTexture::GRASS,
                ],
                [
                    'height' => 0,
                    'texture' => GroundTexture::PATH,
                ],
                [
                    'height' => 0,
                    'texture' => GroundTexture::WATER,
                ],
            ],
            [
                [
                    'height' => 1,
                    'texture' => GroundTexture::ROCK,
                ],
                [
                    'height' => 0,
                    'texture' => GroundTexture::PATH,
                ],
                [
                    'height' => 1,
                    'texture' => GroundTexture::GRASS,
                ],
            ],
            [
                [
                    'height' => 0,
                    'texture' => GroundTexture::GRASS,
                ],
                [
                    'height' => 0,
                    'texture' => GroundTexture::PATH,
                ],
                [
                    'height' => 1,
                    'texture' => GroundTexture::ROCK,
                ],
            ],
        ];

        yield 'Different sections' => [
            '0:grass^0:grass;0:path;1:rock^1:rock;0:water^0:water;0:dirt;0:path;0:grass<highlands>',
            'highlands',
            [
                [
                    'height' => 0,
                    'texture' => GroundTexture::GRASS,
                ],
            ],
            [
                [
                    'height' => 1,
                    'texture' => GroundTexture::ROCK,
                ],
                [
                    'height' => 0,
                    'texture' => GroundTexture::PATH,
                ],
                [
                    'height' => 0,
                    'texture' => GroundTexture::GRASS,
                ],
            ],
            [
                [
                    'height' => 0,
                    'texture' => GroundTexture::WATER,
                ],
                [
                    'height' => 1,
                    'texture' => GroundTexture::ROCK,
                ],
            ],
            [
                [
                    'height' => 0,
                    'texture' => GroundTexture::GRASS,
                ],
                [
                    'height' => 0,
                    'texture' => GroundTexture::PATH,
                ],
                [
                    'height' => 0,
                    'texture' => GroundTexture::DIRT,
                ],
                [
                    'height' => 0,
                    'texture' => GroundTexture::WATER,
                ],
            ],
        ];
    }
}
