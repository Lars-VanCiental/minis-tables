<?php

declare(strict_types=1);

namespace LVC\MinisTablesTests\Domain\Table\Builder;

use LVC\MinisTables\Domain\GroundSection;
use LVC\MinisTables\Domain\Piece;
use LVC\MinisTables\Domain\Side;
use LVC\MinisTables\Domain\Table;
use LVC\MinisTables\Domain\Table\Builder\Builder;
use LVC\MinisTables\Domain\Table\Builder\PiecesSupply;
use LVC\MinisTables\Domain\Table\Builder\PieceValidator;
use LVC\MinisTables\Domain\TableDimensions;
use PHPUnit\Framework\TestCase;

class BuilderTest extends TestCase
{
    /** @var array<Piece> */
    private readonly array $pieces;

    protected function setUp(): void
    {
        $this->pieces = [
            new Piece(
                'test 1 variation',
                new Side(new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0)),
            ),
            new Piece(
                'test 2 variations',
                new Side(new GroundSection('testA', 0)),
                new Side(new GroundSection('testB', 0)),
                new Side(new GroundSection('testA', 0)),
                new Side(new GroundSection('testB', 0)),
            ),
            new Piece(
                'test 4 variations',
                new Side(new GroundSection('testA', 0)),
                new Side(new GroundSection('testB', 0)),
                new Side(new GroundSection('testC', 0)),
                new Side(new GroundSection('testD', 0)),
            ),
        ];
    }

    /**
     * @dataProvider getExpectedTablesForDimensionsTestCases
     *
     * @param array<Table> $expectedTables
     */
    public function testGetAllTables(TableDimensions $dimensions, array $expectedTables): void
    {
        $builder = new Builder($this->getPieceValidatorMock(fn (): bool => true));

        $tables = $builder->createAllTables(new PiecesSupply(...$this->pieces), $dimensions);

        self::assertEqualsCanonicalizing($expectedTables, $tables);
    }

    /**
     * @return \Generator<string, array{TableDimensions, array<Table>}>
     */
    public function getExpectedTablesForDimensionsTestCases(): \Generator
    {
        $piece1 = new Piece(
            'test 1 variation',
            new Side(new GroundSection('test', 0)),
            new Side(new GroundSection('test', 0)),
            new Side(new GroundSection('test', 0)),
            new Side(new GroundSection('test', 0)),
        );
        $piece2 = new Piece(
            'test 2 variations',
            new Side(new GroundSection('testA', 0)),
            new Side(new GroundSection('testB', 0)),
            new Side(new GroundSection('testA', 0)),
            new Side(new GroundSection('testB', 0)),
        );
        $piece4 = new Piece(
            'test 4 variations',
            new Side(new GroundSection('testA', 0)),
            new Side(new GroundSection('testB', 0)),
            new Side(new GroundSection('testC', 0)),
            new Side(new GroundSection('testD', 0)),
        );

        yield 'dimension 1x1' => [
            $dimensions1x1 = new TableDimensions(1, 1),
            [
                new Table($dimensions1x1, $piece1),
                new Table($dimensions1x1, $piece2),
                new Table($dimensions1x1, $piece4),
            ],
        ];

        yield 'dimension 1x2' => [
            $dimensions1x2 = new TableDimensions(1, 2),
            [
                new Table($dimensions1x2, $piece1, $piece2),
                new Table($dimensions1x2, $piece1, $piece2->rotateLeft()),
                new Table($dimensions1x2, $piece1, $piece4),
                new Table($dimensions1x2, $piece1, $piece4->rotateLeft()),
                new Table($dimensions1x2, $piece1, $piece4->rotateLeft()->rotateLeft()),
                new Table($dimensions1x2, $piece1, $piece4->rotateLeft()->rotateLeft()->rotateLeft()),
                new Table($dimensions1x2, $piece2, $piece4),
                new Table($dimensions1x2, $piece2, $piece4->rotateLeft()),
                new Table($dimensions1x2, $piece2, $piece4->rotateLeft()->rotateLeft()),
                new Table($dimensions1x2, $piece2, $piece4->rotateLeft()->rotateLeft()->rotateLeft()),
                new Table($dimensions1x2, $piece2->rotateLeft(), $piece4),
                new Table($dimensions1x2, $piece2->rotateLeft(), $piece4->rotateLeft()),
                new Table($dimensions1x2, $piece2->rotateLeft(), $piece4->rotateLeft()->rotateLeft()),
                new Table($dimensions1x2, $piece2->rotateLeft(), $piece4->rotateLeft()->rotateLeft()->rotateLeft()),
            ],
        ];

        yield 'dimension 3x1' => [
            $dimensions3x1 = new TableDimensions(3, 1),
            [
                new Table($dimensions3x1, $piece1, $piece2, $piece4),
                new Table($dimensions3x1, $piece1, $piece2, $piece4->rotateLeft()),
                new Table($dimensions3x1, $piece1, $piece2, $piece4->rotateLeft()->rotateLeft()),
                new Table($dimensions3x1, $piece1, $piece2, $piece4->rotateLeft()->rotateLeft()->rotateLeft()),
                new Table($dimensions3x1, $piece1, $piece2->rotateLeft(), $piece4),
                new Table($dimensions3x1, $piece1, $piece2->rotateLeft(), $piece4->rotateLeft()),
                new Table($dimensions3x1, $piece1, $piece2->rotateLeft(), $piece4->rotateLeft()->rotateLeft()),
                new Table($dimensions3x1, $piece1, $piece2->rotateLeft(), $piece4->rotateLeft()->rotateLeft()->rotateLeft()),
                new Table($dimensions3x1, $piece1, $piece4, $piece2),
                new Table($dimensions3x1, $piece1, $piece4, $piece2->rotateLeft()),
                new Table($dimensions3x1, $piece1, $piece4->rotateLeft(), $piece2),
                new Table($dimensions3x1, $piece1, $piece4->rotateLeft(), $piece2->rotateLeft()),
                new Table($dimensions3x1, $piece1, $piece4->rotateLeft()->rotateLeft(), $piece2),
                new Table($dimensions3x1, $piece1, $piece4->rotateLeft()->rotateLeft(), $piece2->rotateLeft()),
                new Table($dimensions3x1, $piece1, $piece4->rotateLeft()->rotateLeft()->rotateLeft(), $piece2),
                new Table($dimensions3x1, $piece1, $piece4->rotateLeft()->rotateLeft()->rotateLeft(), $piece2->rotateLeft()),
                new Table($dimensions3x1, $piece2, $piece1, $piece4),
                new Table($dimensions3x1, $piece2, $piece1, $piece4->rotateLeft()),
                new Table($dimensions3x1, $piece2, $piece1, $piece4->rotateLeft()->rotateLeft()),
                new Table($dimensions3x1, $piece2, $piece1, $piece4->rotateLeft()->rotateLeft()->rotateLeft()),
                new Table($dimensions3x1, $piece2->rotateLeft(), $piece1, $piece4),
                new Table($dimensions3x1, $piece2->rotateLeft(), $piece1, $piece4->rotateLeft()),
                new Table($dimensions3x1, $piece2->rotateLeft(), $piece1, $piece4->rotateLeft()->rotateLeft()),
                new Table($dimensions3x1, $piece2->rotateLeft(), $piece1, $piece4->rotateLeft()->rotateLeft()->rotateLeft()),
            ],
        ];

        yield 'dimension 2x2' => [
            new TableDimensions(2, 2),
            [
            ],
        ];
    }

    public function testGetTablesAvoidDuplication(): void
    {
        $piece1 = new Piece(
            'test 1',
            new Side(new GroundSection('test', 0)),
            new Side(new GroundSection('test', 0)),
            new Side(new GroundSection('test', 0)),
            new Side(new GroundSection('test', 0)),
        );
        $piece2 = new Piece(
            'test 2',
            new Side(new GroundSection('test1', 1)),
            new Side(new GroundSection('test1', 1)),
            new Side(new GroundSection('test1', 1)),
            new Side(new GroundSection('test1', 1)),
        );

        $dimensions = new TableDimensions(2, 2);

        $builder = new Builder($this->getPieceValidatorMock(fn (): bool => true));

        $tables = $builder->createAllTables(new PiecesSupply($piece1, $piece1, $piece2, $piece2), $dimensions);

        self::assertCount(2, $tables);
        self::assertEqualsCanonicalizing(
            [
                new Table(
                    $dimensions,
                    $piece1,
                    $piece1,
                    $piece2,
                    $piece2,
                ),
                new Table(
                    $dimensions,
                    $piece1,
                    $piece2,
                    $piece2,
                    $piece1,
                ),
            ],
            $tables
        );
    }

    public function getPieceValidatorMock(callable $returns): PieceValidator
    {
        $mock = self::createMock(PieceValidator::class);

        $mock->method('canPieceBeAdded')->willReturnCallback($returns);

        return $mock;
    }
}
