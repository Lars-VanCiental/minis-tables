<?php

declare(strict_types=1);

namespace LVC\MinisTablesTests\Domain\Table\Builder\PieceValidator;

use LVC\MinisTables\Domain\GroundSection;
use LVC\MinisTables\Domain\Piece;
use LVC\MinisTables\Domain\Side;
use LVC\MinisTables\Domain\Table\Builder\PieceValidator\ValidatePieceByTextureMatching;
use LVC\MinisTables\Domain\TableDimensions;

class ValidatePieceByTextureMatchingTest extends AbstractPieceValidatorTest
{
    public function getCanBePlacedTestCases(): \Generator
    {
        $testAAASide = new Side(new GroundSection('testA', 0), new GroundSection('testA', 1), new GroundSection('testA', 2));
        $testBAASide = new Side(new GroundSection('testB', 3), new GroundSection('testA', 4), new GroundSection('testA', 5));
        $testAABSide = new Side(new GroundSection('testA', 6), new GroundSection('testA', 7), new GroundSection('testB', 8));

        yield 'no pieces picked' => [
            new TableDimensions(2, 2),
            new Piece('test', $testAAASide, $testAAASide, $testAAASide, $testAAASide),
            new ValidatePieceByTextureMatching(),
            [],
        ];

        yield 'test beside same texture piece' => [
            new TableDimensions(2, 2),
            new Piece('test', $testAAASide, $testAAASide, $testAAASide, $testAAASide),
            new ValidatePieceByTextureMatching(),
            [
                new Piece('test', $testAAASide, $testAAASide, $testAAASide, $testAAASide),
            ],
        ];

        yield 'test beside opposed texture' => [
            new TableDimensions(2, 2),
            new Piece('test', $testBAASide, $testBAASide, $testBAASide, $testBAASide),
            new ValidatePieceByTextureMatching(),
            [
                new Piece('test', $testAABSide, $testAABSide, $testAABSide, $testAABSide),
            ],
            [],
        ];

        yield 'test above same texture piece' => [
            new TableDimensions(2, 2),
            new Piece('test', $testAAASide, $testAAASide, $testAAASide, $testAAASide),
            new ValidatePieceByTextureMatching(),
            [
                new Piece('test', $testAAASide, $testAAASide, $testAAASide, $testAAASide),
                new Piece('test', $testAAASide, $testAAASide, $testAAASide, $testAAASide),
            ],
            [],
        ];

        yield 'test above opposed texture piece' => [
            new TableDimensions(2, 2),
            new Piece('test', $testAABSide, $testAABSide, $testAABSide, $testAABSide),
            new ValidatePieceByTextureMatching(),
            [
                new Piece('test', $testBAASide, $testBAASide, $testBAASide, $testBAASide),
                new Piece('test', $testAABSide, $testAABSide, $testAABSide, $testAABSide),
            ],
            [],
        ];

        yield 'test beside and above same texture piece' => [
            new TableDimensions(2, 2),
            new Piece('test', $testAAASide, $testAAASide, $testAAASide, $testAAASide),
            new ValidatePieceByTextureMatching(),
            [
                new Piece('test', $testAAASide, $testAAASide, $testAAASide, $testAAASide),
                new Piece('test', $testAAASide, $testAAASide, $testAAASide, $testAAASide),
                new Piece('test', $testAAASide, $testAAASide, $testAAASide, $testAAASide),
            ],
            [],
        ];

        yield 'test beside and above opposed texture piece' => [
            new TableDimensions(2, 2),
            new Piece('test', $testBAASide, $testBAASide, $testBAASide, $testBAASide),
            new ValidatePieceByTextureMatching(),
            [
                new Piece('test', $testBAASide, $testBAASide, $testBAASide, $testBAASide),
                new Piece('test', $testAABSide, $testAABSide, $testAABSide, $testAABSide),
                new Piece('test', $testAABSide, $testAABSide, $testAABSide, $testAABSide),
            ],
            [],
        ];
    }

    public function getCannotBePlacedTestCases(): \Generator
    {
        $testAAASide = new Side(new GroundSection('testA', 0), new GroundSection('testA', 1), new GroundSection('testA', 2));
        $testBAASide = new Side(new GroundSection('testB', 3), new GroundSection('testA', 4), new GroundSection('testA', 5));
        $testAABSide = new Side(new GroundSection('testA', 6), new GroundSection('testA', 7), new GroundSection('testB', 8));

        yield 'test beside first piece' => [
            new TableDimensions(2, 2),
            new Piece('test', $testAAASide, $testAAASide, $testAAASide, $testAAASide),
            new ValidatePieceByTextureMatching(),
            [
                new Piece('test', $testBAASide, $testBAASide, $testBAASide, $testBAASide),
            ],
            [],
        ];

        yield 'test above piece' => [
            new TableDimensions(2, 2),
            new Piece('test', $testAAASide, $testAAASide, $testAAASide, $testAAASide),
            new ValidatePieceByTextureMatching(),
            [
                new Piece('test', $testBAASide, $testBAASide, $testBAASide, $testBAASide),
                new Piece('test', $testBAASide, $testBAASide, $testBAASide, $testBAASide),
            ],
            [],
        ];

        yield 'test beside and above piece' => [
            new TableDimensions(2, 2),
            new Piece('test', $testAAASide, $testAAASide, $testAAASide, $testAAASide),
            new ValidatePieceByTextureMatching(),
            [
                new Piece('test', $testBAASide, $testBAASide, $testBAASide, $testBAASide),
                new Piece('test', $testAABSide, $testAABSide, $testAABSide, $testAABSide),
                new Piece('test', $testAABSide, $testAABSide, $testAABSide, $testAABSide),
            ],
            [],
        ];
    }
}
