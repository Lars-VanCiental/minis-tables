<?php

declare(strict_types=1);

namespace LVC\MinisTablesTests\Domain\Table\Builder\PieceValidator;

use LVC\MinisTables\Domain\Piece;
use LVC\MinisTables\Domain\Table\Builder\PieceValidator;
use LVC\MinisTables\Domain\Table\Builder\TableBuilding;
use LVC\MinisTables\Domain\TableDimensions;
use PHPUnit\Framework\TestCase;

abstract class AbstractPieceValidatorTest extends TestCase
{
    /**
     * @dataProvider getCanBePlacedTestCases
     *
     * @param array<Piece> $existingPieces
     */
    public function testCanBePlaced(
        TableDimensions $dimensions,
        Piece $testedPiece,
        PieceValidator $pieceValidator,
        array $existingPieces = [],
    ): void {
        self::assertTrue(
            $pieceValidator->canPieceBeAdded(
                $testedPiece,
                $this->getTableBuilding($dimensions, ...$existingPieces),
            )
        );
    }

    /**
     * @return \Generator<string,array{0: TableDimensions, 1: Piece, 2: PieceValidator, 3: array<Piece>}>
     */
    abstract public function getCanBePlacedTestCases(): \Generator;

    /**
     * @dataProvider getCannotBePlacedTestCases
     *
     * @param array<Piece> $existingPieces
     */
    public function testCannotBePlaced(
        TableDimensions $dimensions,
        Piece $testedPiece,
        PieceValidator $pieceValidator,
        array $existingPieces,
    ): void {
        self::assertFalse(
            $pieceValidator->canPieceBeAdded(
                $testedPiece,
                $this->getTableBuilding($dimensions, ...$existingPieces),
            )
        );
    }

    /**
     * @return \Generator<string,array{0: TableDimensions, 1: Piece, 2: PieceValidator, 3: array<Piece>}>
     */
    abstract public function getCannotBePlacedTestCases(): \Generator;

    private function getTableBuilding(TableDimensions $dimensions, Piece ...$pieces): TableBuilding
    {
        $tableBuilding = TableBuilding::startBuilding($dimensions);

        foreach ($pieces as $piece) {
            $tableBuilding = $tableBuilding->addPiece($piece);
        }

        return $tableBuilding;
    }
}
