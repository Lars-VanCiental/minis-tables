<?php

declare(strict_types=1);

namespace LVC\MinisTablesTests\Domain\Table\Builder\PieceValidator;

use LVC\MinisTables\Domain\GroundSection;
use LVC\MinisTables\Domain\Piece;
use LVC\MinisTables\Domain\Side;
use LVC\MinisTables\Domain\Table\Builder\PieceValidator\ValidatePieceByHeightMatching;
use LVC\MinisTables\Domain\TableDimensions;

class ValidatePieceByHeightMatchingTest extends AbstractPieceValidatorTest
{
    public function getCanBePlacedTestCases(): \Generator
    {
        $test000Side = new Side(new GroundSection('testA', 0), new GroundSection('testB', 0), new GroundSection('testC', 0));
        $test100Side = new Side(new GroundSection('testD', 1), new GroundSection('testE', 0), new GroundSection('testF', 0));
        $test001Side = new Side(new GroundSection('testG', 0), new GroundSection('testH', 0), new GroundSection('testI', 1));

        yield 'no pieces picked' => [
            new TableDimensions(2, 2),
            new Piece('test', $test000Side, $test000Side, $test000Side, $test000Side),
            new ValidatePieceByHeightMatching(),
            [],
        ];

        yield 'test beside same height piece' => [
            new TableDimensions(2, 2),
            new Piece('test', $test000Side, $test000Side, $test000Side, $test000Side),
            new ValidatePieceByHeightMatching(),
            [
                new Piece('test', $test000Side, $test000Side, $test000Side, $test000Side),
            ],
        ];

        yield 'test beside opposed height' => [
            new TableDimensions(2, 2),
            new Piece('test', $test100Side, $test100Side, $test100Side, $test100Side),
            new ValidatePieceByHeightMatching(),
            [
                new Piece('test', $test001Side, $test001Side, $test001Side, $test001Side),
            ],
            [],
        ];

        yield 'test above same height' => [
            new TableDimensions(2, 2),
            new Piece('test', $test000Side, $test000Side, $test000Side, $test000Side),
            new ValidatePieceByHeightMatching(),
            [
                new Piece('test', $test000Side, $test000Side, $test000Side, $test000Side),
                new Piece('test', $test000Side, $test000Side, $test000Side, $test000Side),
            ],
            [],
        ];

        yield 'test above opposed height' => [
            new TableDimensions(2, 2),
            new Piece('test', $test001Side, $test001Side, $test001Side, $test001Side),
            new ValidatePieceByHeightMatching(),
            [
                new Piece('test', $test100Side, $test100Side, $test100Side, $test100Side),
                new Piece('test', $test001Side, $test001Side, $test001Side, $test001Side),
            ],
            [],
        ];

        yield 'test beside and above same height' => [
            new TableDimensions(2, 2),
            new Piece('test', $test000Side, $test000Side, $test000Side, $test000Side),
            new ValidatePieceByHeightMatching(),
            [
                new Piece('test', $test000Side, $test000Side, $test000Side, $test000Side),
                new Piece('test', $test000Side, $test000Side, $test000Side, $test000Side),
                new Piece('test', $test000Side, $test000Side, $test000Side, $test000Side),
            ],
            [],
        ];

        yield 'test beside and above opposed height' => [
            new TableDimensions(2, 2),
            new Piece('test', $test100Side, $test100Side, $test100Side, $test100Side),
            new ValidatePieceByHeightMatching(),
            [
                new Piece('test', $test100Side, $test100Side, $test100Side, $test100Side),
                new Piece('test', $test001Side, $test001Side, $test001Side, $test001Side),
                new Piece('test', $test001Side, $test001Side, $test001Side, $test001Side),
            ],
            [],
        ];
    }

    public function getCannotBePlacedTestCases(): \Generator
    {
        $test000Side = new Side(new GroundSection('test', 0), new GroundSection('test', 0), new GroundSection('test', 0));
        $test100Side = new Side(new GroundSection('test', 1), new GroundSection('test', 0), new GroundSection('test', 0));
        $test001Side = new Side(new GroundSection('test', 0), new GroundSection('test', 0), new GroundSection('test', 1));

        yield 'test beside first piece' => [
            new TableDimensions(2, 2),
            new Piece('test', $test000Side, $test000Side, $test000Side, $test000Side),
            new ValidatePieceByHeightMatching(),
            [
                new Piece('test', $test100Side, $test100Side, $test100Side, $test100Side),
            ],
            [],
        ];

        yield 'test above piece' => [
            new TableDimensions(2, 2),
            new Piece('test', $test000Side, $test000Side, $test000Side, $test000Side),
            new ValidatePieceByHeightMatching(),
            [
                new Piece('test', $test100Side, $test100Side, $test100Side, $test100Side),
                new Piece('test', $test100Side, $test100Side, $test100Side, $test100Side),
            ],
            [],
        ];

        yield 'test beside and above piece' => [
            new TableDimensions(2, 2),
            new Piece('test', $test000Side, $test000Side, $test000Side, $test000Side),
            new ValidatePieceByHeightMatching(),
            [
                new Piece('test', $test100Side, $test100Side, $test100Side, $test100Side),
                new Piece('test', $test001Side, $test001Side, $test001Side, $test001Side),
                new Piece('test', $test001Side, $test001Side, $test001Side, $test001Side),
            ],
            [],
        ];
    }
}
