<?php

declare(strict_types=1);

namespace LVC\MinisTablesTests\Domain\Table\Builder\PieceValidator;

use LVC\MinisTables\Domain\GroundSection;
use LVC\MinisTables\Domain\Piece;
use LVC\MinisTables\Domain\Side;
use LVC\MinisTables\Domain\Table\Builder\PieceValidator\ValidatePieceByFullSideMatching;
use LVC\MinisTables\Domain\TableDimensions;

class ValidatePieceByFullSideMatchingTest extends AbstractPieceValidatorTest
{
    public function getCanBePlacedTestCases(): \Generator
    {
        $aaaSide = new Side(new GroundSection('testA', 0), new GroundSection('testA', 0), new GroundSection('testA', 0));
        $AbaSide = new Side(new GroundSection('testA', 1), new GroundSection('testB', 0), new GroundSection('testA', 0));
        $abASide = new Side(new GroundSection('testA', 0), new GroundSection('testB', 0), new GroundSection('testA', 1));

        yield 'no pieces picked' => [
            new TableDimensions(2, 2),
            new Piece('test', $aaaSide, $aaaSide, $aaaSide, $aaaSide),
            new ValidatePieceByFullSideMatching(),
            [],
        ];

        yield 'test beside first same piece' => [
            new TableDimensions(2, 2),
            new Piece('test', $aaaSide, $aaaSide, $aaaSide, $aaaSide),
            new ValidatePieceByFullSideMatching(),
            [
                new Piece('test', $aaaSide, $aaaSide, $aaaSide, $aaaSide),
            ],
        ];

        yield 'test beside first opposed piece' => [
            new TableDimensions(2, 2),
            new Piece('test', $aaaSide, $aaaSide, $aaaSide, $AbaSide),
            new ValidatePieceByFullSideMatching(),
            [
                new Piece('test', $aaaSide, $abASide, $aaaSide, $aaaSide),
            ],
        ];

        yield 'test above same piece' => [
            new TableDimensions(2, 2),
            new Piece('test', $aaaSide, $aaaSide, $aaaSide, $aaaSide),
            new ValidatePieceByFullSideMatching(),
            [
                new Piece('test', $aaaSide, $aaaSide, $aaaSide, $aaaSide),
                new Piece('test', $aaaSide, $aaaSide, $aaaSide, $aaaSide),
            ],
        ];

        yield 'test above opposed piece' => [
            new TableDimensions(2, 2),
            new Piece('test', $abASide, $aaaSide, $aaaSide, $aaaSide),
            new ValidatePieceByFullSideMatching(),
            [
                new Piece('test', $aaaSide, $aaaSide, $AbaSide, $aaaSide),
                new Piece('test', $aaaSide, $aaaSide, $aaaSide, $aaaSide),
            ],
        ];

        yield 'test beside and above same piece' => [
            new TableDimensions(2, 2),
            new Piece('test', $aaaSide, $aaaSide, $aaaSide, $aaaSide),
            new ValidatePieceByFullSideMatching(),
            [
                new Piece('test', $aaaSide, $aaaSide, $aaaSide, $aaaSide),
                new Piece('test', $aaaSide, $aaaSide, $aaaSide, $aaaSide),
                new Piece('test', $aaaSide, $aaaSide, $aaaSide, $aaaSide),
            ],
        ];

        yield 'test beside and above opposed piece' => [
            new TableDimensions(2, 2),
            new Piece('test', $abASide, $aaaSide, $aaaSide, $abASide),
            new ValidatePieceByFullSideMatching(),
            [
                new Piece('test', $aaaSide, $aaaSide, $aaaSide, $aaaSide),
                new Piece('test', $aaaSide, $aaaSide, $AbaSide, $aaaSide),
                new Piece('test', $aaaSide, $AbaSide, $aaaSide, $aaaSide),
            ],
        ];
    }

    public function getCannotBePlacedTestCases(): \Generator
    {
        $aaaSide = new Side(new GroundSection('testA', 0), new GroundSection('testA', 0), new GroundSection('testA', 0));
        $abASide = new Side(new GroundSection('testA', 0), new GroundSection('testB', 0), new GroundSection('testA', 1));

        yield 'test beside first piece' => [
            new TableDimensions(2, 2),
            new Piece('test', $aaaSide, $aaaSide, $aaaSide, $abASide),
            new ValidatePieceByFullSideMatching(),
            [
                new Piece('test', $aaaSide, $aaaSide, $aaaSide, $aaaSide),
            ],
        ];

        yield 'test above piece' => [
            new TableDimensions(2, 2),
            new Piece('test', $abASide, $aaaSide, $aaaSide, $aaaSide),
            new ValidatePieceByFullSideMatching(),
            [
                new Piece('test', $aaaSide, $aaaSide, $aaaSide, $aaaSide),
                new Piece('test', $aaaSide, $aaaSide, $aaaSide, $aaaSide),
            ],
        ];

        yield 'test beside and above piece' => [
            new TableDimensions(2, 2),
            new Piece('test', $abASide, $aaaSide, $aaaSide, $abASide),
            new ValidatePieceByFullSideMatching(),
            [
                new Piece('test', $aaaSide, $aaaSide, $aaaSide, $aaaSide),
                new Piece('test', $aaaSide, $aaaSide, $aaaSide, $aaaSide),
                new Piece('test', $aaaSide, $aaaSide, $aaaSide, $aaaSide),
            ],
        ];
    }
}
