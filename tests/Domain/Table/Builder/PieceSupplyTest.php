<?php

declare(strict_types=1);

namespace LVC\MinisTablesTests\Domain\Table\Builder;

use LVC\MinisTables\Domain\GroundSection;
use LVC\MinisTables\Domain\Piece;
use LVC\MinisTables\Domain\Side;
use LVC\MinisTables\Domain\Table\Builder\PiecesSupply;
use LVC\MinisTables\Domain\Table\Builder\SelectedPiece;
use PHPUnit\Framework\TestCase;

class PieceSupplyTest extends TestCase
{
    /** @var array<Piece> */
    private readonly array $pieces;

    public function setUp(): void
    {
        $this->pieces = [
            new Piece(
                'test 1 variation',
                new Side(new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0)),
            ),
            new Piece(
                'test 2 variations',
                new Side(new GroundSection('testA', 0)),
                new Side(new GroundSection('testB', 0)),
                new Side(new GroundSection('testA', 0)),
                new Side(new GroundSection('testB', 0)),
            ),
            new Piece(
                'test 4 variations',
                new Side(new GroundSection('testA', 0)),
                new Side(new GroundSection('testB', 0)),
                new Side(new GroundSection('testC', 0)),
                new Side(new GroundSection('testD', 0)),
            ),
        ];
    }

    public function testIsEmpty(): void
    {
        $pieceSupply = new PiecesSupply();

        self::assertTrue($pieceSupply->isEmpty);
    }

    public function testIsNotEmpty(): void
    {
        $pieceSupply = new PiecesSupply(...$this->pieces);

        self::assertFalse($pieceSupply->isEmpty);
    }

    public function testGetPiecesToTest(): void
    {
        $pieceSupply = new PiecesSupply(...$this->pieces);

        self::assertInstanceOf(\Generator::class, $selectedPieces = $pieceSupply->selectPieces());

        self::assertInstanceOf(SelectedPiece::class, $selectedPieces->current());
        self::assertSame($this->pieces[0], $selectedPieces->current()->piece);
        self::assertSame([$this->pieces[1], $this->pieces[2]], $selectedPieces->current()->nextPiecesSupply->pieces);

        $selectedPieces->next();

        self::assertInstanceOf(SelectedPiece::class, $selectedPieces->current());
        self::assertSame($this->pieces[1], $selectedPieces->current()->piece);
        self::assertSame([$this->pieces[0], $this->pieces[2]], $selectedPieces->current()->nextPiecesSupply->pieces);

        $selectedPieces->next();

        self::assertInstanceOf(SelectedPiece::class, $selectedPieces->current());
        self::assertSame($this->pieces[1]->getAllVariations()[1], $selectedPieces->current()->piece);
        self::assertSame([$this->pieces[0], $this->pieces[2]], $selectedPieces->current()->nextPiecesSupply->pieces);

        $selectedPieces->next();

        self::assertInstanceOf(SelectedPiece::class, $selectedPieces->current());
        self::assertSame($this->pieces[2], $selectedPieces->current()->piece);
        self::assertSame([$this->pieces[0], $this->pieces[1]], $selectedPieces->current()->nextPiecesSupply->pieces);

        $selectedPieces->next();

        self::assertInstanceOf(SelectedPiece::class, $selectedPieces->current());
        self::assertSame($this->pieces[2]->getAllVariations()[1], $selectedPieces->current()->piece);
        self::assertSame([$this->pieces[0], $this->pieces[1]], $selectedPieces->current()->nextPiecesSupply->pieces);

        $selectedPieces->next();

        self::assertInstanceOf(SelectedPiece::class, $selectedPieces->current());
        self::assertSame($this->pieces[2]->getAllVariations()[2], $selectedPieces->current()->piece);
        self::assertSame([$this->pieces[0], $this->pieces[1]], $selectedPieces->current()->nextPiecesSupply->pieces);

        $selectedPieces->next();

        self::assertInstanceOf(SelectedPiece::class, $selectedPieces->current());
        self::assertSame($this->pieces[2]->getAllVariations()[3], $selectedPieces->current()->piece);
        self::assertSame([$this->pieces[0], $this->pieces[1]], $selectedPieces->current()->nextPiecesSupply->pieces);
    }

    public function testCanHaveMultipleIdenticalPieces(): void
    {
        $pieceSupply = new PiecesSupply($this->pieces[0], $this->pieces[0], $this->pieces[0]);

        self::assertInstanceOf(\Generator::class, $selectedPieces = $pieceSupply->selectPieces());

        self::assertInstanceOf(SelectedPiece::class, $selectedPieces->current());
        self::assertSame($this->pieces[0], $selectedPieces->current()->piece);
        self::assertSame([$this->pieces[0], $this->pieces[0]], $selectedPieces->current()->nextPiecesSupply->pieces);
    }
}
