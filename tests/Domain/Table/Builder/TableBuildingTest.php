<?php

declare(strict_types=1);

namespace LVC\MinisTablesTests\Domain\Table\Builder;

use LVC\MinisTables\Domain\GroundSection;
use LVC\MinisTables\Domain\Piece;
use LVC\MinisTables\Domain\Side;
use LVC\MinisTables\Domain\Table\Builder\TableBuilding;
use LVC\MinisTables\Domain\TableDimensions;
use PHPUnit\Framework\TestCase;

class TableBuildingTest extends TestCase
{
    public function testStart(): void
    {
        $tableBuilding = TableBuilding::startBuilding(new TableDimensions(2, 2));

        self::assertFalse($tableBuilding->isComplete);
    }

    public function testGetTableFailsWhileTableIsNotComplete(): void
    {
        $tableBuilding = TableBuilding::startBuilding(new TableDimensions(2, 2));

        self::assertFalse($tableBuilding->isComplete);

        self::expectException(\LogicException::class);
        self::expectExceptionMessage('Cannot retrieve table, it is not yet complete.');

        $tableBuilding->getTable();
    }

    public function testAddPiece(): void
    {
        $tableBuilding = TableBuilding::startBuilding(new TableDimensions(2, 2));

        self::assertFalse($tableBuilding->isComplete);

        $nextTableBuilding = $tableBuilding->addPiece(
            new Piece(
                'test',
                new Side(new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0)),
            ),
        );

        self::assertNotSame($tableBuilding, $nextTableBuilding);
        self::assertNotEqualsCanonicalizing($tableBuilding, $nextTableBuilding);

        self::assertFalse($nextTableBuilding->isComplete);
    }

    public function testTableIsComplete(): void
    {
        $tableBuilding = TableBuilding::startBuilding($dimensions = new TableDimensions(2, 2));
        $completeTableBuilding = $tableBuilding->addPiece(
            $piece1 = new Piece(
                'test',
                new Side(new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0)),
            ),
        )->addPiece(
            $piece2 = new Piece(
                'test',
                new Side(new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0)),
            ),
        )->addPiece(
            $piece3 = new Piece(
                'test',
                new Side(new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0)),
            ),
        )->addPiece(
            $piece4 = new Piece(
                'test',
                new Side(new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0)),
            ),
        );

        self::assertFalse($tableBuilding->isComplete);
        self::assertTrue($completeTableBuilding->isComplete);

        $table = $completeTableBuilding->getTable();

        self::assertSame($dimensions, $table->dimensions);
        self::assertSame($piece1, $table->getPiece(0, 0));
        self::assertSame($piece2, $table->getPiece(1, 0));
        self::assertSame($piece3, $table->getPiece(0, 1));
        self::assertSame($piece4, $table->getPiece(1, 1));
    }

    public function testAddPieceFailsWhenTableIsAlreadyComplete(): void
    {
        $tableBuilding = TableBuilding::startBuilding($dimensions = new TableDimensions(2, 2));
        $completeTableBuilding = $tableBuilding->addPiece(
            new Piece(
                'test',
                new Side(new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0)),
            ),
        )->addPiece(
            new Piece(
                'test',
                new Side(new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0)),
            ),
        )->addPiece(
            new Piece(
                'test',
                new Side(new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0)),
            ),
        )->addPiece(
            new Piece(
                'test',
                new Side(new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0)),
            ),
        );

        self::expectException(\LogicException::class);
        self::expectExceptionMessage('Cannot add a new piece to a complete table.');

        $completeTableBuilding->addPiece(
            new Piece(
                'test',
                new Side(new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0)),
            ),
        );
    }
}
