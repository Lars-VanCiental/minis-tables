<?php

declare(strict_types=1);

namespace LVC\MinisTablesTests\Domain;

use LVC\MinisTables\Domain\TableDimensions;
use PHPUnit\Framework\TestCase;

class TableDimensionsTest extends TestCase
{
    /**
     * @dataProvider getInvalidDimensionsTestCases
     */
    public function testUnableToCreateWidthInvalidDimensions(int $width, int $length): void
    {
        self::expectException(\InvalidArgumentException::class);

        new TableDimensions($width, $length);
    }

    /**
     * @return \Generator<string,array{int, int}>
     */
    public function getInvalidDimensionsTestCases(): \Generator
    {
        yield '0 width' => [
            0,
            1,
        ];

        yield '0 length' => [
            1,
            0,
        ];

        yield 'lower than 0 width' => [
            -1,
            1,
        ];

        yield 'lower than 0 length' => [
            1,
            -1,
        ];
    }

    public function testSizeForDimensions(): void
    {
        $dimensions = new TableDimensions(2, 3);

        self::assertSame(6, $dimensions->size);
    }

    public function testRotateWithSameWidthLength(): void
    {
        $dimensions = new TableDimensions(2, 2);

        $rotatedDimensions = $dimensions->rotate();
        self::assertSame($dimensions, $rotatedDimensions);
        self::assertEquals(2, $rotatedDimensions->width);
        self::assertEquals(2, $rotatedDimensions->length);
    }

    public function testRotateDifferentWithWidthLength(): void
    {
        $dimensions = new TableDimensions(3, 2);

        $rotatedDimensions = $dimensions->rotate();
        self::assertNotSame($dimensions, $rotatedDimensions);
        self::assertEquals(2, $rotatedDimensions->width);
        self::assertEquals(3, $rotatedDimensions->length);
    }

    public function testRotateIsUnique(): void
    {
        $dimensions = new TableDimensions(3, 2);

        $rotatedDimensions = $dimensions->rotate();
        self::assertNotSame($dimensions, $rotatedDimensions);
        self::assertSame($dimensions->rotate(), $rotatedDimensions);
        self::assertSame($rotatedDimensions->rotate(), $dimensions);
    }
}
