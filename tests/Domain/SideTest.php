<?php

declare(strict_types=1);

namespace LVC\MinisTablesTests\Domain;

use LVC\MinisTables\Domain\GroundSection;
use LVC\MinisTables\Domain\Side;
use PHPUnit\Framework\TestCase;

class SideTest extends TestCase
{
    /**
     * @dataProvider getSideGroundSectionsWithDescriptionsTestCases
     */
    public function testGetDescriptions(
        string $expectedDescriptionFromLeftToRight,
        string $expectedDescriptionFromRightToLeft,
        GroundSection ...$groundSections
    ): void {
        $side = new Side(...$groundSections);

        self::assertSame($expectedDescriptionFromLeftToRight, $side->descriptionFromLeftToRight);
        self::assertSame($expectedDescriptionFromRightToLeft, $side->descriptionFromRightToLeft);
    }

    /**
     * @return \Generator<string,array{0: string, 1: string, 2: GroundSection}>
     */
    public function getSideGroundSectionsWithDescriptionsTestCases(): \Generator
    {
        yield '1 ground section' => [
            '0:test',
            '0:test',
            new GroundSection('test', 0),
        ];

        yield '2 ground section' => [
            '0:test;1:test',
            '1:test;0:test',
            new GroundSection('test', 0),
            new GroundSection('test', 1),
        ];

        yield '3 ground section' => [
            '0:test;1:test;2:test',
            '2:test;1:test;0:test',
            new GroundSection('test', 0),
            new GroundSection('test', 1),
            new GroundSection('test', 2),
        ];

        yield '4 ground section' => [
            '0:test;1:test;2:test;3:test',
            '3:test;2:test;1:test;0:test',
            new GroundSection('test', 0),
            new GroundSection('test', 1),
            new GroundSection('test', 2),
            new GroundSection('test', 3),
        ];

        yield '5 ground section' => [
            '0:test;1:test;2:test;3:test;4:test',
            '4:test;3:test;2:test;1:test;0:test',
            new GroundSection('test', 0),
            new GroundSection('test', 1),
            new GroundSection('test', 2),
            new GroundSection('test', 3),
            new GroundSection('test', 4),
        ];
    }
}
