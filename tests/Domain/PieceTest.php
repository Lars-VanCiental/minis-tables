<?php

declare(strict_types=1);

namespace LVC\MinisTablesTests\Domain;

use LVC\MinisTables\Domain\GroundSection;
use LVC\MinisTables\Domain\Piece;
use LVC\MinisTables\Domain\Side;
use PHPUnit\Framework\TestCase;

class PieceTest extends TestCase
{
    /**
     * @dataProvider getPieceSidesWithDescriptionTestCases
     */
    public function testGetDescriptionAndDescriptionFromTopClockwise(
        string $expectedDescription,
        Side $topSide,
        Side $rightSide,
        Side $bottomSide,
        Side $leftSide
    ): void {
        $piece = new Piece('test', $topSide, $rightSide, $bottomSide, $leftSide);

        self::assertSame($expectedDescription, $piece->descriptionFromTopClockwise);
        self::assertSame($expectedDescription.'<test>', $piece->fullDescription);
    }

    /**
     * @return \Generator<string,array{0: string, 1: Side, 2: Side, 3: Side, 4: Side}>
     */
    public function getPieceSidesWithDescriptionTestCases(): \Generator
    {
        yield '1 ground sections sides' => [
            '0:test^1:test^2:test^3:test',
            new Side(new GroundSection('test', 0)),
            new Side(new GroundSection('test', 1)),
            new Side(new GroundSection('test', 2)),
            new Side(new GroundSection('test', 3)),
        ];

        yield '2 ground sections sides' => [
            '1:test;0:test^3:test;2:test^5:test;4:test^7:test;6:test',
            new Side(new GroundSection('test', 0), new GroundSection('test', 1)),
            new Side(new GroundSection('test', 2), new GroundSection('test', 3)),
            new Side(new GroundSection('test', 4), new GroundSection('test', 5)),
            new Side(new GroundSection('test', 6), new GroundSection('test', 7)),
        ];

        yield 'classical 3 sections sides' => [
            '2:test;1:test;0:test^5:test;4:test;3:test^8:test;7:test;6:test^11:test;10:test;9:test',
            new Side(new GroundSection('test', 0), new GroundSection('test', 1), new GroundSection('test', 2)),
            new Side(new GroundSection('test', 3), new GroundSection('test', 4), new GroundSection('test', 5)),
            new Side(new GroundSection('test', 6), new GroundSection('test', 7), new GroundSection('test', 8)),
            new Side(new GroundSection('test', 9), new GroundSection('test', 10), new GroundSection('test', 11)),
        ];

        yield 'different numbers of sections for each sides' => [
            '0:test^2:test;1:test^6:test;5:test;4:test;3:test^11:test;10:test;9:test;8:test;7:test',
            new Side(new GroundSection('test', 0)),
            new Side(new GroundSection('test', 1), new GroundSection('test', 2)),
            new Side(new GroundSection('test', 3), new GroundSection('test', 4), new GroundSection('test', 5), new GroundSection('test', 6)),
            new Side(new GroundSection('test', 7), new GroundSection('test', 8), new GroundSection('test', 9), new GroundSection('test', 10), new GroundSection('test', 11)),
        ];
    }

    public function testGetAllVariationsWithFourIdenticalSides(): void
    {
        $piece = new Piece(
            'test',
            new Side(new GroundSection('test', 0), new GroundSection('test', 0), new GroundSection('test', 0)),
            new Side(new GroundSection('test', 0), new GroundSection('test', 0), new GroundSection('test', 0)),
            new Side(new GroundSection('test', 0), new GroundSection('test', 0), new GroundSection('test', 0)),
            new Side(new GroundSection('test', 0), new GroundSection('test', 0), new GroundSection('test', 0)),
        );

        $pieceVariations = $piece->getAllVariations();

        self::assertIsArray($pieceVariations);
        self::assertCount(1, $pieceVariations);

        self::assertSame($piece, $pieceVariations[0]);
    }

    public function testGetAllVariationsWithOpposedIdenticalSides(): void
    {
        $piece = new Piece(
            'test',
            new Side(new GroundSection('test', 0), new GroundSection('test', 1), new GroundSection('test', 2)),
            new Side(new GroundSection('test', 3), new GroundSection('test', 4), new GroundSection('test', 5)),
            new Side(new GroundSection('test', 0), new GroundSection('test', 1), new GroundSection('test', 2)),
            new Side(new GroundSection('test', 3), new GroundSection('test', 4), new GroundSection('test', 5)),
        );

        $pieceVariations = $piece->getAllVariations();

        self::assertIsArray($pieceVariations);
        self::assertCount(2, $pieceVariations);

        self::assertSame($piece, $pieceVariations[0]);

        self::assertSame(
            '5:test;4:test;3:test^2:test;1:test;0:test^5:test;4:test;3:test^2:test;1:test;0:test',
            $pieceVariations[1]->descriptionFromTopClockwise
        );
    }

    public function testGetAllVariationsWithTwoAdjacentIdenticalSides(): void
    {
        $piece = new Piece(
            'test',
            new Side(new GroundSection('test', 0), new GroundSection('test', 1), new GroundSection('test', 2)),
            new Side(new GroundSection('test', 0), new GroundSection('test', 1), new GroundSection('test', 2)),
            new Side(new GroundSection('test', 3), new GroundSection('test', 4), new GroundSection('test', 5)),
            new Side(new GroundSection('test', 3), new GroundSection('test', 4), new GroundSection('test', 5)),
        );

        $pieceVariations = $piece->getAllVariations();

        self::assertIsArray($pieceVariations);
        self::assertCount(4, $pieceVariations);

        self::assertSame($piece, $pieceVariations[0]);

        self::assertSame(
            '2:test;1:test;0:test^5:test;4:test;3:test^5:test;4:test;3:test^2:test;1:test;0:test',
            $pieceVariations[1]->descriptionFromTopClockwise
        );

        self::assertSame(
            '5:test;4:test;3:test^5:test;4:test;3:test^2:test;1:test;0:test^2:test;1:test;0:test',
            $pieceVariations[2]->descriptionFromTopClockwise
        );

        self::assertSame(
            '5:test;4:test;3:test^2:test;1:test;0:test^2:test;1:test;0:test^5:test;4:test;3:test',
            $pieceVariations[3]->descriptionFromTopClockwise
        );
    }

    public function testGetAllVariationsWithDifferentSides(): void
    {
        $piece = new Piece(
            'test',
            new Side(new GroundSection('test', 0), new GroundSection('test', 1), new GroundSection('test', 2)),
            new Side(new GroundSection('test', 3), new GroundSection('test', 4), new GroundSection('test', 5)),
            new Side(new GroundSection('test', 6), new GroundSection('test', 7), new GroundSection('test', 8)),
            new Side(new GroundSection('test', 9), new GroundSection('test', 10), new GroundSection('test', 11)),
        );

        $pieceVariations = $piece->getAllVariations();

        self::assertIsArray($pieceVariations);
        self::assertCount(4, $pieceVariations);

        self::assertSame($piece, $pieceVariations[0]);

        self::assertSame(
            '5:test;4:test;3:test^8:test;7:test;6:test^11:test;10:test;9:test^2:test;1:test;0:test',
            $pieceVariations[1]->descriptionFromTopClockwise
        );
        self::assertSame(
            '8:test;7:test;6:test^11:test;10:test;9:test^2:test;1:test;0:test^5:test;4:test;3:test',
            $pieceVariations[2]->descriptionFromTopClockwise
        );
        self::assertSame(
            '11:test;10:test;9:test^2:test;1:test;0:test^5:test;4:test;3:test^8:test;7:test;6:test',
            $pieceVariations[3]->descriptionFromTopClockwise
        );
    }

    public function testRotateLeftWithFourIdenticalSides(): void
    {
        $piece = new Piece(
            'test',
            new Side(new GroundSection('test', 0), new GroundSection('test', 0), new GroundSection('test', 0)),
            new Side(new GroundSection('test', 0), new GroundSection('test', 0), new GroundSection('test', 0)),
            new Side(new GroundSection('test', 0), new GroundSection('test', 0), new GroundSection('test', 0)),
            new Side(new GroundSection('test', 0), new GroundSection('test', 0), new GroundSection('test', 0)),
        );

        $rotated90 = $piece->rotateLeft();

        self::assertSame($piece, $rotated90);
    }

    public function testRotateLeft(): void
    {
        $piece = new Piece(
            'test',
            new Side(new GroundSection('test', 0), new GroundSection('test', 1), new GroundSection('test', 2)),
            new Side(new GroundSection('test', 3), new GroundSection('test', 4), new GroundSection('test', 5)),
            new Side(new GroundSection('test', 6), new GroundSection('test', 7), new GroundSection('test', 8)),
            new Side(new GroundSection('test', 9), new GroundSection('test', 10), new GroundSection('test', 11)),
        );

        $rotated90 = $piece->rotateLeft();
        $rotated180 = $rotated90->rotateLeft();
        $rotated270 = $rotated180->rotateLeft();
        $rotated360 = $rotated270->rotateLeft();

        self::assertSame(
            '5:test;4:test;3:test^8:test;7:test;6:test^11:test;10:test;9:test^2:test;1:test;0:test',
            $rotated90->descriptionFromTopClockwise
        );
        self::assertSame(
            '8:test;7:test;6:test^11:test;10:test;9:test^2:test;1:test;0:test^5:test;4:test;3:test',
            $rotated180->descriptionFromTopClockwise
        );
        self::assertSame(
            '11:test;10:test;9:test^2:test;1:test;0:test^5:test;4:test;3:test^8:test;7:test;6:test',
            $rotated270->descriptionFromTopClockwise
        );
        self::assertSame($piece, $rotated360);
    }
}
