<?php

declare(strict_types=1);

namespace LVC\MinisTablesTests\Domain;

use LVC\MinisTables\Domain\GroundSection;
use LVC\MinisTables\Domain\Piece;
use LVC\MinisTables\Domain\Side;
use LVC\MinisTables\Domain\Table;
use LVC\MinisTables\Domain\TableDimensions;
use PHPUnit\Framework\TestCase;

class TableTest extends TestCase
{
    public function testUnableToBuildWithoutRightAmountOfPieces(): void
    {
        self::expectException(\InvalidArgumentException::class);
        self::expectExceptionMessage('For a 1*2 table, you need exactly 2 pieces.');

        new Table(
            new TableDimensions(1, 2),
            new Piece(
                'test',
                new Side(new GroundSection('test', 0), new GroundSection('ground', 0), new GroundSection('test', 0)),
                new Side(new GroundSection('test', 1), new GroundSection('test', 0), new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0), new GroundSection('ground', 0), new GroundSection('test', 1)),
                new Side(new GroundSection('test', 0), new GroundSection('ground', 0), new GroundSection('test', 1))
            )
        );
    }

    public function testGetUnavailablePieceX(): void
    {
        $table = new Table(
            new TableDimensions(2, 2),
            $firstPiece = new Piece(
                'test',
                new Side(new GroundSection('test', 0), new GroundSection('ground', 0), new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0), new GroundSection('ground', 0), new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0), new GroundSection('ground', 0), new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0), new GroundSection('ground', 0), new GroundSection('test', 0))
            ),
            $secondPiece = new Piece(
                'test',
                new Side(new GroundSection('test', 1), new GroundSection('ground', 0), new GroundSection('test', 0)),
                new Side(new GroundSection('test', 1), new GroundSection('ground', 0), new GroundSection('test', 0)),
                new Side(new GroundSection('test', 1), new GroundSection('ground', 0), new GroundSection('test', 0)),
                new Side(new GroundSection('test', 1), new GroundSection('ground', 0), new GroundSection('test', 0))
            ),
            $thirdPiece = new Piece(
                'test',
                new Side(new GroundSection('test', 0), new GroundSection('ground', 0), new GroundSection('test', 1)),
                new Side(new GroundSection('test', 0), new GroundSection('ground', 0), new GroundSection('test', 1)),
                new Side(new GroundSection('test', 0), new GroundSection('ground', 0), new GroundSection('test', 1)),
                new Side(new GroundSection('test', 0), new GroundSection('ground', 0), new GroundSection('test', 1))
            ),
            $fourthPiece = new Piece(
                'test',
                new Side(new GroundSection('test', 0), new GroundSection('test', 0), new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0), new GroundSection('test', 0), new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0), new GroundSection('test', 0), new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0), new GroundSection('test', 0), new GroundSection('test', 0))
            )
        );

        self::expectException(\InvalidArgumentException::class);
        self::expectExceptionMessage('Cannot ask for a piece out of the table.');

        self::assertSame($firstPiece, $table->getPiece(3, 2));
    }

    public function testGetUnavailablePieceY(): void
    {
        $table = new Table(
            new TableDimensions(2, 2),
            $firstPiece = new Piece(
                'test',
                new Side(new GroundSection('test', 0), new GroundSection('ground', 0), new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0), new GroundSection('ground', 0), new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0), new GroundSection('ground', 0), new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0), new GroundSection('ground', 0), new GroundSection('test', 0))
            ),
            $secondPiece = new Piece(
                'test',
                new Side(new GroundSection('test', 1), new GroundSection('ground', 0), new GroundSection('test', 0)),
                new Side(new GroundSection('test', 1), new GroundSection('ground', 0), new GroundSection('test', 0)),
                new Side(new GroundSection('test', 1), new GroundSection('ground', 0), new GroundSection('test', 0)),
                new Side(new GroundSection('test', 1), new GroundSection('ground', 0), new GroundSection('test', 0))
            ),
            $thirdPiece = new Piece(
                'test',
                new Side(new GroundSection('test', 0), new GroundSection('ground', 0), new GroundSection('test', 1)),
                new Side(new GroundSection('test', 0), new GroundSection('ground', 0), new GroundSection('test', 1)),
                new Side(new GroundSection('test', 0), new GroundSection('ground', 0), new GroundSection('test', 1)),
                new Side(new GroundSection('test', 0), new GroundSection('ground', 0), new GroundSection('test', 1))
            ),
            $fourthPiece = new Piece(
                'test',
                new Side(new GroundSection('test', 0), new GroundSection('test', 0), new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0), new GroundSection('test', 0), new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0), new GroundSection('test', 0), new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0), new GroundSection('test', 0), new GroundSection('test', 0))
            )
        );

        self::expectException(\InvalidArgumentException::class);
        self::expectExceptionMessage('Cannot ask for a piece out of the table.');

        self::assertSame($firstPiece, $table->getPiece(2, 3));
    }

    public function testGetPiece(): void
    {
        $table = new Table(
            new TableDimensions(2, 2),
            $firstPiece = new Piece(
                'test',
                new Side(new GroundSection('test', 0), new GroundSection('ground', 0), new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0), new GroundSection('ground', 0), new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0), new GroundSection('ground', 0), new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0), new GroundSection('ground', 0), new GroundSection('test', 0))
            ),
            $secondPiece = new Piece(
                'test',
                new Side(new GroundSection('test', 1), new GroundSection('ground', 0), new GroundSection('test', 0)),
                new Side(new GroundSection('test', 1), new GroundSection('ground', 0), new GroundSection('test', 0)),
                new Side(new GroundSection('test', 1), new GroundSection('ground', 0), new GroundSection('test', 0)),
                new Side(new GroundSection('test', 1), new GroundSection('ground', 0), new GroundSection('test', 0))
            ),
            $thirdPiece = new Piece(
                'test',
                new Side(new GroundSection('test', 0), new GroundSection('ground', 0), new GroundSection('test', 1)),
                new Side(new GroundSection('test', 0), new GroundSection('ground', 0), new GroundSection('test', 1)),
                new Side(new GroundSection('test', 0), new GroundSection('ground', 0), new GroundSection('test', 1)),
                new Side(new GroundSection('test', 0), new GroundSection('ground', 0), new GroundSection('test', 1))
            ),
            $fourthPiece = new Piece(
                'test',
                new Side(new GroundSection('test', 0), new GroundSection('test', 0), new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0), new GroundSection('test', 0), new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0), new GroundSection('test', 0), new GroundSection('test', 0)),
                new Side(new GroundSection('test', 0), new GroundSection('test', 0), new GroundSection('test', 0))
            )
        );

        self::assertSame($firstPiece, $table->getPiece(0, 0));
        self::assertSame($thirdPiece, $table->getPiece(0, 1));
        self::assertSame($secondPiece, $table->getPiece(1, 0));
        self::assertSame($fourthPiece, $table->getPiece(1, 1));
    }

    /**
     * @dataProvider getUniqueDescriptionTestCases
     */
    public function testUniqueDescription(Table $table, string $expectedDescription): void
    {
        self::assertSame($expectedDescription, $table->uniqueDescription);
    }

    /**
     * @return \Generator<string, array{Table, string}>
     */
    public function getUniqueDescriptionTestCases(): \Generator
    {
        yield '1 by 1 table' => [
            new Table(
                new TableDimensions(1, 1),
                new Piece(
                    'test',
                    new Side(new GroundSection('test', 1)),
                    new Side(new GroundSection('test', 2)),
                    new Side(new GroundSection('test', 3)),
                    new Side(new GroundSection('test', 4)),
                ),
            ),
            '1:test^2:test^3:test^4:test<test>',
        ];

        yield '1 by 1 table -90°' => [
            new Table(
                new TableDimensions(1, 1),
                new Piece(
                    'test',
                    new Side(new GroundSection('test', 2)),
                    new Side(new GroundSection('test', 3)),
                    new Side(new GroundSection('test', 4)),
                    new Side(new GroundSection('test', 1)),
                ),
            ),
            '1:test^2:test^3:test^4:test<test>',
        ];

        yield '1 by 1 table -180°' => [
            new Table(
                new TableDimensions(1, 1),
                new Piece(
                    'test',
                    new Side(new GroundSection('test', 3)),
                    new Side(new GroundSection('test', 4)),
                    new Side(new GroundSection('test', 1)),
                    new Side(new GroundSection('test', 2)),
                ),
            ),
            '1:test^2:test^3:test^4:test<test>',
        ];

        yield '1 by 1 table -270°' => [
            new Table(
                new TableDimensions(1, 1),
                new Piece(
                    'test',
                    new Side(new GroundSection('test', 4)),
                    new Side(new GroundSection('test', 1)),
                    new Side(new GroundSection('test', 2)),
                    new Side(new GroundSection('test', 3)),
                ),
            ),
            '1:test^2:test^3:test^4:test<test>',
        ];

        yield '2 by 1 table' => [
            new Table(
                new TableDimensions(2, 1),
                new Piece(
                    'test1',
                    new Side(new GroundSection('testA', 0), new GroundSection('testB', 0)),
                    new Side(new GroundSection('testA', 1), new GroundSection('testB', 1)),
                    new Side(new GroundSection('testA', 0), new GroundSection('testB', 0)),
                    new Side(new GroundSection('testA', 0), new GroundSection('testB', 0)),
                ),
                new Piece(
                    'test2',
                    new Side(new GroundSection('testA', 0), new GroundSection('testB', 0)),
                    new Side(new GroundSection('testA', 0), new GroundSection('testB', 0)),
                    new Side(new GroundSection('testA', 0), new GroundSection('testB', 0)),
                    new Side(new GroundSection('testA', 1), new GroundSection('testB', 1)),
                ),
            ),
            '0:testB;0:testA^0:testB;0:testA^1:testB;1:testA^0:testB;0:testA<test1>'
            .PHP_EOL
            .'1:testB;1:testA^0:testB;0:testA^0:testB;0:testA^0:testB;0:testA<test2>',
        ];

        yield '2 by 1 table -90°' => [
            new Table(
                new TableDimensions(1, 2),
                new Piece(
                    'test2',
                    new Side(new GroundSection('testA', 0), new GroundSection('testB', 0)),
                    new Side(new GroundSection('testA', 0), new GroundSection('testB', 0)),
                    new Side(new GroundSection('testA', 1), new GroundSection('testB', 1)),
                    new Side(new GroundSection('testA', 0), new GroundSection('testB', 0)),
                ),
                new Piece(
                    'test1',
                    new Side(new GroundSection('testA', 1), new GroundSection('testB', 1)),
                    new Side(new GroundSection('testA', 0), new GroundSection('testB', 0)),
                    new Side(new GroundSection('testA', 0), new GroundSection('testB', 0)),
                    new Side(new GroundSection('testA', 0), new GroundSection('testB', 0)),
                ),
            ),
            '0:testB;0:testA^0:testB;0:testA^1:testB;1:testA^0:testB;0:testA<test1>'
            .PHP_EOL
            .'1:testB;1:testA^0:testB;0:testA^0:testB;0:testA^0:testB;0:testA<test2>',
        ];

        yield '2 by 1 table -180°' => [
            new Table(
                new TableDimensions(2, 1),
                new Piece(
                    'test2',
                    new Side(new GroundSection('testA', 0), new GroundSection('testB', 0)),
                    new Side(new GroundSection('testA', 1), new GroundSection('testB', 1)),
                    new Side(new GroundSection('testA', 0), new GroundSection('testB', 0)),
                    new Side(new GroundSection('testA', 0), new GroundSection('testB', 0)),
                ),
                new Piece(
                    'test1',
                    new Side(new GroundSection('testA', 0), new GroundSection('testB', 0)),
                    new Side(new GroundSection('testA', 0), new GroundSection('testB', 0)),
                    new Side(new GroundSection('testA', 0), new GroundSection('testB', 0)),
                    new Side(new GroundSection('testA', 1), new GroundSection('testB', 1)),
                ),
            ),
            '0:testB;0:testA^0:testB;0:testA^1:testB;1:testA^0:testB;0:testA<test1>'
            .PHP_EOL
            .'1:testB;1:testA^0:testB;0:testA^0:testB;0:testA^0:testB;0:testA<test2>',
        ];

        yield '2 by 1 table -270°' => [
            new Table(
                new TableDimensions(1, 2),
                new Piece(
                    'test2',
                    new Side(new GroundSection('testA', 0), new GroundSection('testB', 0)),
                    new Side(new GroundSection('testA', 0), new GroundSection('testB', 0)),
                    new Side(new GroundSection('testA', 1), new GroundSection('testB', 1)),
                    new Side(new GroundSection('testA', 0), new GroundSection('testB', 0)),
                ),
                new Piece(
                    'test1',
                    new Side(new GroundSection('testA', 1), new GroundSection('testB', 1)),
                    new Side(new GroundSection('testA', 0), new GroundSection('testB', 0)),
                    new Side(new GroundSection('testA', 0), new GroundSection('testB', 0)),
                    new Side(new GroundSection('testA', 0), new GroundSection('testB', 0)),
                ),
            ),
            '0:testB;0:testA^0:testB;0:testA^1:testB;1:testA^0:testB;0:testA<test1>'
            .PHP_EOL
            .'1:testB;1:testA^0:testB;0:testA^0:testB;0:testA^0:testB;0:testA<test2>',
        ];

        yield '4 by 2 table' => [
            new Table(
                new TableDimensions(4, 2),
                new Piece(
                    'test1',
                    new Side(new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 0), new GroundSection('testB', 0), new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 0), new GroundSection('testB', 0), new GroundSection('testA', 0)),
                ),
                new Piece(
                    'test2',
                    new Side(new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 1), new GroundSection('testB', 0), new GroundSection('testA', 1)),
                    new Side(new GroundSection('testA', 0), new GroundSection('testB', 0), new GroundSection('testA', 0)),
                ),
                new Piece(
                    'test3',
                    new Side(new GroundSection('testC', 1), new GroundSection('testA', 0)),
                    new Side(new GroundSection('testC', 1)),
                    new Side(new GroundSection('testA', 0), new GroundSection('testC', 1)),
                    new Side(new GroundSection('testA', 0)),
                ),
                new Piece(
                    'test4',
                    new Side(new GroundSection('testA', 0), new GroundSection('testC', 1)),
                    new Side(new GroundSection('testA', 0)),
                    new Side(new GroundSection('testC', 1), new GroundSection('testA', 0)),
                    new Side(new GroundSection('testC', 1)),
                ),
                new Piece(
                    'test5',
                    new Side(new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 0)),
                ),
                new Piece(
                    'test6',
                    new Side(new GroundSection('testA', 1), new GroundSection('testB', 0), new GroundSection('testA', 1)),
                    new Side(new GroundSection('testA', 0), new GroundSection('testB', 0), new GroundSection('testA', 1)),
                    new Side(new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 0)),
                ),
                new Piece(
                    'test7',
                    new Side(new GroundSection('testC', 1), new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 0), new GroundSection('testC', 1)),
                    new Side(new GroundSection('testA', 0), new GroundSection('testB', 0), new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 1), new GroundSection('testB', 0), new GroundSection('testA', 0)),
                ),
                new Piece(
                    'test8',
                    new Side(new GroundSection('testA', 0), new GroundSection('testC', 1)),
                    new Side(new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 0)),
                    new Side(new GroundSection('testC', 1), new GroundSection('testA', 0)),
                ),
            ),
            '0:testA^0:testA;0:testB;0:testA^0:testA^0:testA;0:testB;0:testA<test1>'
            .'!'
            .'0:testA^0:testA^1:testA;0:testB;1:testA^0:testA;0:testB;0:testA<test2>'
            .'!'
            .'0:testA;1:testC^1:testC^1:testC;0:testA^0:testA<test3>'
            .'!'
            .'1:testC;0:testA^0:testA^0:testA;1:testC^1:testC<test4>'
            .PHP_EOL
            .'0:testA^0:testA^0:testA^0:testA<test5>'
            .'!'
            .'1:testA;0:testB;1:testA^1:testA;0:testB;0:testA^0:testA^0:testA<test6>'
            .'!'
            .'0:testA;1:testC^1:testC;0:testA^0:testA;0:testB;0:testA^0:testA;0:testB;1:testA<test7>'
            .'!'
            .'1:testC;0:testA^0:testA^0:testA^0:testA;1:testC<test8>',
        ];

        yield '4 by 2 table -90°' => [
            new Table(
                new TableDimensions(2, 4),
                new Piece(
                    'test4',
                    new Side(new GroundSection('testA', 0)),
                    new Side(new GroundSection('testC', 1), new GroundSection('testA', 0)),
                    new Side(new GroundSection('testC', 1)),
                    new Side(new GroundSection('testA', 0), new GroundSection('testC', 1)),
                ),
                new Piece(
                    'test8',
                    new Side(new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 0)),
                    new Side(new GroundSection('testC', 1), new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 0), new GroundSection('testC', 1)),
                ),
                new Piece(
                    'test3',
                    new Side(new GroundSection('testC', 1)),
                    new Side(new GroundSection('testA', 0), new GroundSection('testC', 1)),
                    new Side(new GroundSection('testA', 0)),
                    new Side(new GroundSection('testC', 1), new GroundSection('testA', 0)),
                ),
                new Piece(
                    'test7',
                    new Side(new GroundSection('testA', 0), new GroundSection('testC', 1)),
                    new Side(new GroundSection('testA', 0), new GroundSection('testB', 0), new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 1), new GroundSection('testB', 0), new GroundSection('testA', 0)),
                    new Side(new GroundSection('testC', 1), new GroundSection('testA', 0)),
                ),
                new Piece(
                    'test2',
                    new Side(new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 1), new GroundSection('testB', 0), new GroundSection('testA', 1)),
                    new Side(new GroundSection('testA', 0), new GroundSection('testB', 0), new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 0)),
                ),
                new Piece(
                    'test6',
                    new Side(new GroundSection('testA', 0), new GroundSection('testB', 0), new GroundSection('testA', 1)),
                    new Side(new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 1), new GroundSection('testB', 0), new GroundSection('testA', 1)),
                ),
                new Piece(
                    'test1',
                    new Side(new GroundSection('testA', 0), new GroundSection('testB', 0), new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 0), new GroundSection('testB', 0), new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 0)),
                ),
                new Piece(
                    'test5',
                    new Side(new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 0)),
                ),
            ),
            '0:testA^0:testA;0:testB;0:testA^0:testA^0:testA;0:testB;0:testA<test1>'
            .'!'
            .'0:testA^0:testA^1:testA;0:testB;1:testA^0:testA;0:testB;0:testA<test2>'
            .'!'
            .'0:testA;1:testC^1:testC^1:testC;0:testA^0:testA<test3>'
            .'!'
            .'1:testC;0:testA^0:testA^0:testA;1:testC^1:testC<test4>'
            .PHP_EOL
            .'0:testA^0:testA^0:testA^0:testA<test5>'
            .'!'
            .'1:testA;0:testB;1:testA^1:testA;0:testB;0:testA^0:testA^0:testA<test6>'
            .'!'
            .'0:testA;1:testC^1:testC;0:testA^0:testA;0:testB;0:testA^0:testA;0:testB;1:testA<test7>'
            .'!'
            .'1:testC;0:testA^0:testA^0:testA^0:testA;1:testC<test8>',
        ];

        yield '4 by 2 table -180°' => [
            new Table(
                new TableDimensions(4, 2),
                new Piece(
                    'test8',
                    new Side(new GroundSection('testA', 0)),
                    new Side(new GroundSection('testC', 1), new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 0), new GroundSection('testC', 1)),
                    new Side(new GroundSection('testA', 0)),
                ),
                new Piece(
                    'test7',
                    new Side(new GroundSection('testA', 0), new GroundSection('testB', 0), new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 1), new GroundSection('testB', 0), new GroundSection('testA', 0)),
                    new Side(new GroundSection('testC', 1), new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 0), new GroundSection('testC', 1)),
                ),
                new Piece(
                    'test6',
                    new Side(new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 1), new GroundSection('testB', 0), new GroundSection('testA', 1)),
                    new Side(new GroundSection('testA', 0), new GroundSection('testB', 0), new GroundSection('testA', 1)),
                ),
                new Piece(
                    'test5',
                    new Side(new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 0)),
                ),
                new Piece(
                    'test4',
                    new Side(new GroundSection('testC', 1), new GroundSection('testA', 0)),
                    new Side(new GroundSection('testC', 1)),
                    new Side(new GroundSection('testA', 0), new GroundSection('testC', 1)),
                    new Side(new GroundSection('testA', 0)),
                ),
                new Piece(
                    'test3',
                    new Side(new GroundSection('testA', 0), new GroundSection('testC', 1)),
                    new Side(new GroundSection('testA', 0)),
                    new Side(new GroundSection('testC', 1), new GroundSection('testA', 0)),
                    new Side(new GroundSection('testC', 1)),
                ),
                new Piece(
                    'test2',
                    new Side(new GroundSection('testA', 1), new GroundSection('testB', 0), new GroundSection('testA', 1)),
                    new Side(new GroundSection('testA', 0), new GroundSection('testB', 0), new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 0)),
                ),
                new Piece(
                    'test1',
                    new Side(new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 0), new GroundSection('testB', 0), new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 0), new GroundSection('testB', 0), new GroundSection('testA', 0)),
                ),
            ),
            '0:testA^0:testA;0:testB;0:testA^0:testA^0:testA;0:testB;0:testA<test1>'
            .'!'
            .'0:testA^0:testA^1:testA;0:testB;1:testA^0:testA;0:testB;0:testA<test2>'
            .'!'
            .'0:testA;1:testC^1:testC^1:testC;0:testA^0:testA<test3>'
            .'!'
            .'1:testC;0:testA^0:testA^0:testA;1:testC^1:testC<test4>'
            .PHP_EOL
            .'0:testA^0:testA^0:testA^0:testA<test5>'
            .'!'
            .'1:testA;0:testB;1:testA^1:testA;0:testB;0:testA^0:testA^0:testA<test6>'
            .'!'
            .'0:testA;1:testC^1:testC;0:testA^0:testA;0:testB;0:testA^0:testA;0:testB;1:testA<test7>'
            .'!'
            .'1:testC;0:testA^0:testA^0:testA^0:testA;1:testC<test8>',
        ];

        yield '4 by 2 table -270°' => [
            new Table(
                new TableDimensions(2, 4),
                new Piece(
                    'test5',
                    new Side(new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 0)),
                ),
                new Piece(
                    'test1',
                    new Side(new GroundSection('testA', 0), new GroundSection('testB', 0), new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 0), new GroundSection('testB', 0), new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 0)),
                ),
                new Piece(
                    'test6',
                    new Side(new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 1), new GroundSection('testB', 0), new GroundSection('testA', 1)),
                    new Side(new GroundSection('testA', 0), new GroundSection('testB', 0), new GroundSection('testA', 1)),
                    new Side(new GroundSection('testA', 0)),
                ),
                new Piece(
                    'test2',
                    new Side(new GroundSection('testA', 0), new GroundSection('testB', 0), new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 1), new GroundSection('testB', 0), new GroundSection('testA', 1)),
                ),
                new Piece(
                    'test7',
                    new Side(new GroundSection('testA', 1), new GroundSection('testB', 0), new GroundSection('testA', 0)),
                    new Side(new GroundSection('testC', 1), new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 0), new GroundSection('testC', 1)),
                    new Side(new GroundSection('testA', 0), new GroundSection('testB', 0), new GroundSection('testA', 0)),
                ),
                new Piece(
                    'test3',
                    new Side(new GroundSection('testA', 0)),
                    new Side(new GroundSection('testC', 1), new GroundSection('testA', 0)),
                    new Side(new GroundSection('testC', 1)),
                    new Side(new GroundSection('testA', 0), new GroundSection('testC', 1)),
                ),
                new Piece(
                    'test8',
                    new Side(new GroundSection('testC', 1), new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 0), new GroundSection('testC', 1)),
                    new Side(new GroundSection('testA', 0)),
                    new Side(new GroundSection('testA', 0)),
                ),
                new Piece(
                    'test4',
                    new Side(new GroundSection('testC', 1)),
                    new Side(new GroundSection('testA', 0), new GroundSection('testC', 1)),
                    new Side(new GroundSection('testA', 0)),
                    new Side(new GroundSection('testC', 1), new GroundSection('testA', 0)),
                ),
            ),
            '0:testA^0:testA;0:testB;0:testA^0:testA^0:testA;0:testB;0:testA<test1>'
            .'!'
            .'0:testA^0:testA^1:testA;0:testB;1:testA^0:testA;0:testB;0:testA<test2>'
            .'!'
            .'0:testA;1:testC^1:testC^1:testC;0:testA^0:testA<test3>'
            .'!'
            .'1:testC;0:testA^0:testA^0:testA;1:testC^1:testC<test4>'
            .PHP_EOL
            .'0:testA^0:testA^0:testA^0:testA<test5>'
            .'!'
            .'1:testA;0:testB;1:testA^1:testA;0:testB;0:testA^0:testA^0:testA<test6>'
            .'!'
            .'0:testA;1:testC^1:testC;0:testA^0:testA;0:testB;0:testA^0:testA;0:testB;1:testA<test7>'
            .'!'
            .'1:testC;0:testA^0:testA^0:testA^0:testA;1:testC<test8>',
        ];
    }
}
