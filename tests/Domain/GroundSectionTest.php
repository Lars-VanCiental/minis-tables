<?php

declare(strict_types=1);

namespace LVC\MinisTablesTests\Domain;

use LVC\MinisTables\Domain\GroundSection;
use PHPUnit\Framework\TestCase;

class GroundSectionTest extends TestCase
{
    public function testGetDescription(): void
    {
        $section = new GroundSection('test', 1);

        self::assertSame($section->description, '1:test');
    }
}
