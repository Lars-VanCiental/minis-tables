<?php

declare(strict_types=1);

namespace LVC\MinisTablesTests\Application;

use LVC\MinisTables\Application\DiscoverTablesAction;
use LVC\MinisTables\Application\TableBuildingInstructions;
use LVC\MinisTables\Application\TablesRepository;
use LVC\MinisTables\Domain\GroundSection;
use LVC\MinisTables\Domain\Piece;
use LVC\MinisTables\Domain\Side;
use LVC\MinisTables\Domain\Table;
use LVC\MinisTables\Domain\Table\Builder\PieceValidator;
use LVC\MinisTables\Domain\TableDimensions;
use PHPUnit\Framework\TestCase;

class DiscoverTablesActionTest extends TestCase
{
    public function testAction(): void
    {
        $piece1 = new Piece(
            'test 1 variation',
            new Side(new GroundSection('test', 0)),
            new Side(new GroundSection('test', 0)),
            new Side(new GroundSection('test', 0)),
            new Side(new GroundSection('test', 0)),
        );
        $piece2 = new Piece(
            'test 2 variations',
            new Side(new GroundSection('testA', 0)),
            new Side(new GroundSection('testB', 0)),
            new Side(new GroundSection('testA', 0)),
            new Side(new GroundSection('testB', 0)),
        );
        $piece4 = new Piece(
            'test 4 variations',
            new Side(new GroundSection('testA', 0)),
            new Side(new GroundSection('testB', 0)),
            new Side(new GroundSection('testC', 0)),
            new Side(new GroundSection('testD', 0)),
        );

        $dimensions1x2 = new TableDimensions(1, 2);

        $expectedTables = [
            new Table($dimensions1x2, $piece1, $piece2),
            new Table($dimensions1x2, $piece1, $piece2->rotateLeft()),
            new Table($dimensions1x2, $piece1, $piece4),
            new Table($dimensions1x2, $piece1, $piece4->rotateLeft()),
            new Table($dimensions1x2, $piece1, $piece4->rotateLeft()->rotateLeft()),
            new Table($dimensions1x2, $piece1, $piece4->rotateLeft()->rotateLeft()->rotateLeft()),
            new Table($dimensions1x2, $piece2, $piece4),
            new Table($dimensions1x2, $piece2, $piece4->rotateLeft()),
            new Table($dimensions1x2, $piece2, $piece4->rotateLeft()->rotateLeft()),
            new Table($dimensions1x2, $piece2, $piece4->rotateLeft()->rotateLeft()->rotateLeft()),
            new Table($dimensions1x2, $piece2->rotateLeft(), $piece4),
            new Table($dimensions1x2, $piece2->rotateLeft(), $piece4->rotateLeft()),
            new Table($dimensions1x2, $piece2->rotateLeft(), $piece4->rotateLeft()->rotateLeft()),
            new Table($dimensions1x2, $piece2->rotateLeft(), $piece4->rotateLeft()->rotateLeft()->rotateLeft()),
        ];

        $pieceValidatorMock = self::createMock(PieceValidator::class);
        $pieceValidatorMock->method('canPieceBeAdded')->willReturn(true);

        $tableBuildingInstructions = new TableBuildingInstructions(
            $dimensions1x2,
            $pieceValidatorMock,
            $piece1,
            $piece2,
            $piece4
        );

        $tablesRepositoryMock = self::createMock(TablesRepository::class);
        $tablesRepositoryMock->expects(self::once())->method('saveTables')->with($tableBuildingInstructions, ...$expectedTables);

        $action = new DiscoverTablesAction($tablesRepositoryMock);

        self::assertEqualsCanonicalizing(
            $expectedTables,
            $action($tableBuildingInstructions)
        );
    }
}
