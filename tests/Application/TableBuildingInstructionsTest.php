<?php

declare(strict_types=1);

namespace LVC\MinisTablesTests\Application;

use LVC\MinisTables\Application\TableBuildingInstructions;
use LVC\MinisTables\Domain\GroundSection;
use LVC\MinisTables\Domain\Piece;
use LVC\MinisTables\Domain\Side;
use LVC\MinisTables\Domain\Table\Builder\PieceValidator;
use LVC\MinisTables\Domain\TableDimensions;
use PHPUnit\Framework\TestCase;

class TableBuildingInstructionsTest extends TestCase
{
    public function testDescription(): void
    {
        $pieceValidatorMock = self::getMockBuilder(PieceValidator::class)
            ->disableOriginalConstructor()
            ->disableOriginalClone()
            ->disableArgumentCloning()
            ->disallowMockingUnknownTypes()
            ->setMockClassName('SomethingValidator')
            ->getMock();

        $piece1 = new Piece(
            'test 1 variation',
            new Side(new GroundSection('test', 0)),
            new Side(new GroundSection('test', 0)),
            new Side(new GroundSection('test', 0)),
            new Side(new GroundSection('test', 0)),
        );
        $piece2 = new Piece(
            'test 2 variations',
            new Side(new GroundSection('testA', 0)),
            new Side(new GroundSection('testB', 0)),
            new Side(new GroundSection('testA', 0)),
            new Side(new GroundSection('testB', 0)),
        );

        $tableBuildingInstructions = new TableBuildingInstructions(
            new TableDimensions(1, 2),
            $pieceValidatorMock,
            $piece1,
            $piece2
        );

        self::assertSame(
            'SomethingValidator[1x2]0:test^0:test^0:test^0:test<test 1 variation>.0:testA^0:testB^0:testA^0:testB<test 2 variations>',
            $tableBuildingInstructions->description
        );
    }
}
