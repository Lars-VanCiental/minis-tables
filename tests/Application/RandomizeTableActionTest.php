<?php

declare(strict_types=1);

namespace LVC\MinisTablesTests\Application;

use LVC\MinisTables\Application\NotEnoughTables;
use LVC\MinisTables\Application\RandomizeTablesAction;
use LVC\MinisTables\Application\TableBuildingInstructions;
use LVC\MinisTables\Application\TablesRepository;
use LVC\MinisTables\Application\UndiscoveredTables;
use LVC\MinisTables\Domain\GroundSection;
use LVC\MinisTables\Domain\Piece;
use LVC\MinisTables\Domain\Side;
use LVC\MinisTables\Domain\Table;
use LVC\MinisTables\Domain\Table\Builder\PieceValidator;
use LVC\MinisTables\Domain\TableDimensions;
use PHPUnit\Framework\TestCase;

class RandomizeTableActionTest extends TestCase
{
    public function testActionFailsWithNegativeNumber(): void
    {
        self::expectException(\RuntimeException::class);
        self::expectExceptionMessage('Number of tables must be a positive integer.');

        $dimensions1x1 = new TableDimensions(1, 1);

        $pieceValidatorMock = self::createMock(PieceValidator::class);
        $pieceValidatorMock->method('canPieceBeAdded')->willReturn(true);

        $tableBuildingInstructions = new TableBuildingInstructions(
            $dimensions1x1,
            $pieceValidatorMock,
        );

        $tablesRepositoryMock = self::createMock(TablesRepository::class);

        $action = new RandomizeTablesAction($tablesRepositoryMock);
        $tables = $action($tableBuildingInstructions, 0);
    }

    public function testActionFailsWithUndiscoveredTables(): void
    {
        self::expectException(UndiscoveredTables::class);
        self::expectExceptionMessage('Tables needs to be discovered before being randomized.');

        $dimensions1x1 = new TableDimensions(1, 1);

        $pieceValidatorMock = self::createMock(PieceValidator::class);
        $pieceValidatorMock->method('canPieceBeAdded')->willReturn(true);

        $tableBuildingInstructions = new TableBuildingInstructions(
            $dimensions1x1,
            $pieceValidatorMock,
        );

        $tablesRepositoryMock = self::createMock(TablesRepository::class);
        $tablesRepositoryMock->expects(self::once())->method('getTables')->with($tableBuildingInstructions)->willReturn(null);

        $action = new RandomizeTablesAction($tablesRepositoryMock);
        $tables = $action($tableBuildingInstructions, 1);
    }

    public function testActionFailsWithNotEnoughTables(): void
    {
        self::expectException(NotEnoughTables::class);
        self::expectExceptionMessage('Not enough tables with different pieces to select 5 on them.');

        $piece1 = new Piece(
            'test 1 variation',
            new Side(new GroundSection('test', 0)),
            new Side(new GroundSection('test', 0)),
            new Side(new GroundSection('test', 0)),
            new Side(new GroundSection('test', 0)),
        );
        $piece2 = new Piece(
            'test 2 variations',
            new Side(new GroundSection('testA', 0)),
            new Side(new GroundSection('testB', 0)),
            new Side(new GroundSection('testA', 0)),
            new Side(new GroundSection('testB', 0)),
        );
        $piece4 = new Piece(
            'test 4 variations',
            new Side(new GroundSection('testA', 0)),
            new Side(new GroundSection('testB', 0)),
            new Side(new GroundSection('testC', 0)),
            new Side(new GroundSection('testD', 0)),
        );

        $dimensions1x1 = new TableDimensions(1, 1);

        $tables = [
            new Table($dimensions1x1, $piece1),
            new Table($dimensions1x1, $piece2),
            new Table($dimensions1x1, $piece4),
        ];

        $dimensions1x1 = new TableDimensions(1, 1);

        $pieceValidatorMock = self::createMock(PieceValidator::class);
        $pieceValidatorMock->method('canPieceBeAdded')->willReturn(true);

        $tableBuildingInstructions = new TableBuildingInstructions(
            $dimensions1x1,
            $pieceValidatorMock,
            $piece1,
            $piece2,
            $piece4,
        );

        $tablesRepositoryMock = self::createMock(TablesRepository::class);
        $tablesRepositoryMock->expects(self::once())->method('getTables')->with($tableBuildingInstructions)->willReturn($tables);

        $action = new RandomizeTablesAction($tablesRepositoryMock);
        $tables = $action($tableBuildingInstructions, 5);
    }

    public function testAction(): void
    {
        $piece1 = new Piece(
            'test 1 variation',
            new Side(new GroundSection('test', 0)),
            new Side(new GroundSection('test', 0)),
            new Side(new GroundSection('test', 0)),
            new Side(new GroundSection('test', 0)),
        );
        $piece2 = new Piece(
            'test 2 variations',
            new Side(new GroundSection('testA', 0)),
            new Side(new GroundSection('testB', 0)),
            new Side(new GroundSection('testA', 0)),
            new Side(new GroundSection('testB', 0)),
        );
        $piece4 = new Piece(
            'test 4 variations',
            new Side(new GroundSection('testA', 0)),
            new Side(new GroundSection('testB', 0)),
            new Side(new GroundSection('testC', 0)),
            new Side(new GroundSection('testD', 0)),
        );

        $dimensions1x1 = new TableDimensions(1, 1);

        $tables = [
            new Table($dimensions1x1, $piece1),
            new Table($dimensions1x1, $piece2),
            new Table($dimensions1x1, $piece4),
        ];

        $dimensions1x1 = new TableDimensions(1, 1);

        $pieceValidatorMock = self::createMock(PieceValidator::class);
        $pieceValidatorMock->method('canPieceBeAdded')->willReturn(true);

        $tableBuildingInstructions = new TableBuildingInstructions(
            $dimensions1x1,
            $pieceValidatorMock,
            $piece1,
            $piece2,
            $piece4,
        );

        $tablesRepositoryMock = self::createMock(TablesRepository::class);
        $tablesRepositoryMock->expects(self::once())->method('getTables')->with($tableBuildingInstructions)->willReturn($tables);

        $action = new RandomizeTablesAction($tablesRepositoryMock);
        $tables = $action($tableBuildingInstructions, 2);

        self::assertCount(2, $tables);
    }
}
