<?php

require __DIR__.'/../vendor/autoload.php';

use LVC\MinisTables\Infra\Command\TablePresenterCommand;
use LVC\MinisTables\Infra\Command\TableRandomizerCommand;
use LVC\MinisTables\Infra\InMemoryTablesRepository;
use Symfony\Component\Console\Application;

$tablesRepository = new InMemoryTablesRepository();

$application = new Application();

$application->add(new TableRandomizerCommand($tablesRepository));
$application->add(new TablePresenterCommand($tablesRepository));

$application->run();
