# minis-tables #

Generating minis tables and finding the best new piece to build.

## Concept ###

A table is built from pieces. A piece is described from it's four sides.

A side has 3 sections: left, middle and right. A section can either be

 * A grass surface at height 0
 * A grass surface at height 1
 * A road at height 0

## Piece representation ##

To easily represent pieces, the following notation is used:

 * A grass surface at height 0 is: ``0``
 * A grass surface at height 1 is: ``1``
 * A road at height 0 is: ``_``
 * A side is 3 sections, for example: ``0_1``
 * A piece is 4 sections joined by a ``;``, for example: ``0_1;1_1;100;000``
 * A piece is represented from the most top left section and described in clockwise order
 * To input multiple pieces, they're joined by a ``^``, for example ``000;0_1;1_1;0_0^000;000;0_1;1_0^1_1;100;0_0;001^1_0;0_0;000;001`` 

## Usage ##

A bunch of commands are available to use.

### Table Randomizer ###

It will draw a table of the desired size based from the given pieces.

```shell
$ php bin/application.php table:randomize 2 2 "000;0_1;1_1;0_0^000;000;0_1;1_0^1_1;100;0_0;001^1_0;0_0;000;001" --rules full
```

### Table Presented ###

It will draw all tables available of the desired size based from the given pieces.

```shell
$ php bin/application.php table:present 3 1 "000;0_1;1_1;0_0^000;000;0_1;1_0^1_1;100;0_0;001^1_0;0_0;000;001" --rules full --pieces-per-line 15
```

### Piece Finder ###

It will search for the beast piece to craft based on a given set of pieces and a desired size of table.

```shell
$ php bin/application.php piece:find 2 2 "000;0_1;1_1;0_0^000;000;0_1;1_0^1_1;100;0_0;001^1_0;0_0;000;001" --rules full --top-results 15
```
