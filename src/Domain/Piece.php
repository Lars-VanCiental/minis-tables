<?php

declare(strict_types=1);

namespace LVC\MinisTables\Domain;

final class Piece
{
    private const NAME_OPENING_SEPARATOR = '<';
    private const NAME_CLOSING_SEPARATOR = '>';
    private const SIDES_SEPARATOR = '^';

    public readonly string $descriptionFromTopClockwise;
    public readonly string $fullDescription;
    /** @var non-empty-array<self> */
    private array $cachedVariations;

    public function __construct(
        public readonly string $name,
        public readonly Side $topSide,
        public readonly Side $rightSide,
        public readonly Side $bottomSide,
        public readonly Side $leftSide
    ) {
        $this->descriptionFromTopClockwise = implode(
            self::SIDES_SEPARATOR,
            [
                $this->topSide->descriptionFromRightToLeft,
                $this->rightSide->descriptionFromRightToLeft,
                $this->bottomSide->descriptionFromRightToLeft,
                $this->leftSide->descriptionFromRightToLeft,
            ]
        );
        $this->fullDescription = $this->descriptionFromTopClockwise.self::NAME_OPENING_SEPARATOR.$this->name.self::NAME_CLOSING_SEPARATOR;
    }

    /**
     * @return array<self>
     */
    public function getAllVariations(): array
    {
        if (!isset($this->cachedVariations)) {
            $this->buildCachedVariations();
        }

        return $this->cachedVariations;
    }

    public function rotateLeft(): self
    {
        if (!isset($this->cachedVariations)) {
            $this->buildCachedVariations();
        }

        return $this->cachedVariations[1] ?? $this->cachedVariations[0];
    }

    private function buildCachedVariations(): void
    {
        // All side are identical, no variations.
        if (
            $this->topSide->descriptionFromLeftToRight === $this->rightSide->descriptionFromLeftToRight
            && $this->topSide->descriptionFromLeftToRight === $this->bottomSide->descriptionFromLeftToRight
            && $this->topSide->descriptionFromLeftToRight === $this->leftSide->descriptionFromLeftToRight
        ) {
            $this->cachedVariations = [
                $this,
            ];

            return;
        }

        // Both opposed sides are identical, only one variation.
        if (
            $this->topSide->descriptionFromLeftToRight === $this->bottomSide->descriptionFromLeftToRight
            && $this->rightSide->descriptionFromLeftToRight === $this->leftSide->descriptionFromLeftToRight
        ) {
            $variation = new self(
                $this->name,
                $this->rightSide,
                $this->bottomSide,
                $this->leftSide,
                $this->topSide,
            );

            $this->cachedVariations = [
                $this,
                $variation,
            ];
            $variation->cachedVariations = [
                $variation,
                $this,
            ];

            return;
        }

        // Default case, 4 variations.

        $variation2 = new self(
            $this->name,
            $this->rightSide,
            $this->bottomSide,
            $this->leftSide,
            $this->topSide,
        );
        $variation3 = new self(
            $this->name,
            $this->bottomSide,
            $this->leftSide,
            $this->topSide,
            $this->rightSide,
        );
        $variation4 = new self(
            $this->name,
            $this->leftSide,
            $this->topSide,
            $this->rightSide,
            $this->bottomSide,
        );

        $this->cachedVariations = [
            $this,
            $variation2,
            $variation3,
            $variation4,
        ];
        $variation2->cachedVariations = [
            $variation2,
            $variation3,
            $variation4,
            $this,
        ];
        $variation3->cachedVariations = [
            $variation3,
            $variation4,
            $this,
            $variation2,
        ];
        $variation4->cachedVariations = [
            $variation4,
            $this,
            $variation2,
            $variation3,
        ];
    }
}
