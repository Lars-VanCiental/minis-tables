<?php

declare(strict_types=1);

namespace LVC\MinisTables\Domain\Table\Builder;

use LVC\MinisTables\Domain\Piece;

final class PiecesSupply
{
    public readonly bool $isEmpty;
    /** @var array<Piece> */
    public readonly array $pieces;

    public function __construct(Piece ...$pieces)
    {
        $this->pieces = $pieces;
        $this->isEmpty = count($pieces) === 0;
    }

    /**
     * @return \Generator<SelectedPiece>
     */
    public function selectPieces(): \Generator
    {
        foreach ($this->pieces as $position => $piece) {
            $nextPiecesSupplyPieces = $this->pieces;
            array_splice($nextPiecesSupplyPieces, $position, 1);

            $nextPiecesSupply = new PiecesSupply(...$nextPiecesSupplyPieces);

            foreach ($piece->getAllVariations() as $pieceVariation) {
                yield new SelectedPiece($pieceVariation, $nextPiecesSupply);
            }
        }
    }
}
