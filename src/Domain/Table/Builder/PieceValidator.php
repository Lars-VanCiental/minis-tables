<?php

declare(strict_types=1);

namespace LVC\MinisTables\Domain\Table\Builder;

use LVC\MinisTables\Domain\Piece;

interface PieceValidator
{
    public function canPieceBeAdded(
        Piece $testedPiece,
        TableBuilding $tableBuilding,
    ): bool;
}
