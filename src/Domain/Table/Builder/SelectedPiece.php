<?php

declare(strict_types=1);

namespace LVC\MinisTables\Domain\Table\Builder;

use LVC\MinisTables\Domain\Piece;

final class SelectedPiece
{
    public function __construct(
        public readonly Piece $piece,
        public readonly PiecesSupply $nextPiecesSupply,
    ) {
    }
}
