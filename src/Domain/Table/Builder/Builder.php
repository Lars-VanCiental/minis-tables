<?php

declare(strict_types=1);

namespace LVC\MinisTables\Domain\Table\Builder;

use LVC\MinisTables\Domain\Table;
use LVC\MinisTables\Domain\TableDimensions;

final class Builder
{
    public function __construct(
        private readonly PieceValidator $pieceValidator,
    ) {
    }

    /**
     * @return array<Table>
     */
    public function createAllTables(PiecesSupply $piecesSupply, TableDimensions $dimensions): array
    {
        return $this->buildTables(TableBuilding::startBuilding($dimensions), $piecesSupply);
    }

    /**
     * @return array<Table>
     */
    private function buildTables(
        TableBuilding $tableBuilding,
        PiecesSupply $piecesSupply,
    ): array {
        if ($tableBuilding->isComplete) {
            return [$tableBuilding->getTable()];
        }

        if ($piecesSupply->isEmpty) {
            return [];
        }

        /** @var array<Table> $tables */
        $tables = [];

        foreach ($piecesSupply->selectPieces() as $selectedPiece) {
            if ($this->pieceValidator->canPieceBeAdded($selectedPiece->piece, $tableBuilding)) {
                $tables = [
                    ...$tables,
                    ...array_filter(
                        $this->buildTables(
                            $tableBuilding->addPiece($selectedPiece->piece),
                            $selectedPiece->nextPiecesSupply
                        ),
                        function (Table $newTable) use ($tables): bool {
                            foreach ($tables as $existingTable) {
                                if ($newTable->uniqueDescription === $existingTable->uniqueDescription) {
                                    return false;
                                }
                            }

                            return true;
                        },
                    ),
                ];
            }
        }

        return $tables;
    }
}
