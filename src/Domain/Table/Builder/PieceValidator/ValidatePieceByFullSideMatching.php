<?php

declare(strict_types=1);

namespace LVC\MinisTables\Domain\Table\Builder\PieceValidator;

use LVC\MinisTables\Domain\Side;
use LVC\MinisTables\Domain\Table\Builder\PieceValidator;

final class ValidatePieceByFullSideMatching implements PieceValidator
{
    use SidesValidatorHelper;

    private function doesOppositeSidesMatch(Side $sideA, Side $sideB): bool
    {
        return $sideA->descriptionFromLeftToRight === $sideB->descriptionFromRightToLeft;
    }
}
