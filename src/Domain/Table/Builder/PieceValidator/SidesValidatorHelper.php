<?php

declare(strict_types=1);

namespace LVC\MinisTables\Domain\Table\Builder\PieceValidator;

use LVC\MinisTables\Domain\Piece;
use LVC\MinisTables\Domain\Side;
use LVC\MinisTables\Domain\Table\Builder\TableBuilding;

trait SidesValidatorHelper
{
    public function canPieceBeAdded(
        Piece $testedPiece,
        TableBuilding $tableBuilding,
    ): bool {
        $pickedPieces = $tableBuilding->pieces;
        $dimensions = $tableBuilding->dimensions;

        switch (true) {
            case empty($pickedPieces):
                break;

            case count($pickedPieces) < $dimensions->width:
                $lastBesidePickedPiece = end($pickedPieces);

                return $this->doesOppositeSidesMatch($lastBesidePickedPiece->rightSide, $testedPiece->leftSide);

            case count($pickedPieces) % $dimensions->width === 0:
                $lastAbovePickedPiece = $pickedPieces[count($pickedPieces) - $dimensions->width];

                return $this->doesOppositeSidesMatch($lastAbovePickedPiece->bottomSide, $testedPiece->topSide);

            default:
                $lastBesidePickedPiece = end($pickedPieces);
                $lastAbovePickedPiece = $pickedPieces[count($pickedPieces) - $dimensions->width];

                return $this->doesOppositeSidesMatch($lastBesidePickedPiece->rightSide, $testedPiece->leftSide)
                    && $this->doesOppositeSidesMatch($lastAbovePickedPiece->bottomSide, $testedPiece->topSide);
        }

        return true;
    }

    abstract private function doesOppositeSidesMatch(Side $sideA, Side $sideB): bool;
}
