<?php

declare(strict_types=1);

namespace LVC\MinisTables\Domain\Table\Builder\PieceValidator;

use LVC\MinisTables\Domain\GroundSection;
use LVC\MinisTables\Domain\Side;
use LVC\MinisTables\Domain\Table\Builder\PieceValidator;

final class ValidatePieceByTextureMatching implements PieceValidator
{
    use SidesValidatorHelper;

    private function doesOppositeSidesMatch(Side $sideA, Side $sideB): bool
    {
        return self::extractTextures(...$sideA->groundSectionsFromLeftToRight) === self::extractTextures(...array_reverse($sideB->groundSectionsFromLeftToRight));
    }

    private static function extractTextures(GroundSection ...$groundSection): string
    {
        return implode(
            ';',
            array_map(
                fn (GroundSection $groundSection): string => $groundSection->texture,
                $groundSection
            )
        );
    }
}
