<?php

declare(strict_types=1);

namespace LVC\MinisTables\Domain\Table\Builder\PieceValidator;

use LVC\MinisTables\Domain\GroundSection;
use LVC\MinisTables\Domain\Side;
use LVC\MinisTables\Domain\Table\Builder\PieceValidator;

final class ValidatePieceByHeightMatching implements PieceValidator
{
    use SidesValidatorHelper;

    private function doesOppositeSidesMatch(Side $sideA, Side $sideB): bool
    {
        return self::extractHeights(...$sideA->groundSectionsFromLeftToRight) === self::extractHeights(...array_reverse($sideB->groundSectionsFromLeftToRight));
    }

    private static function extractHeights(GroundSection ...$groundSection): string
    {
        return implode(
            ';',
            array_map(
                fn (GroundSection $groundSection): string => (string) $groundSection->height,
                $groundSection
            )
        );
    }
}
