<?php

declare(strict_types=1);

namespace LVC\MinisTables\Domain\Table\Builder;

use LVC\MinisTables\Domain\Piece;
use LVC\MinisTables\Domain\Table;
use LVC\MinisTables\Domain\TableDimensions;

final class TableBuilding
{
    /** @var array<Piece> */
    public readonly array $pieces;
    public readonly bool $isComplete;

    private function __construct(
        public readonly TableDimensions $dimensions,
        Piece ...$pieces
    ) {
        $this->pieces = $pieces;

        $this->isComplete = $this->dimensions->size === count($this->pieces);
    }

    public static function startBuilding(TableDimensions $dimensions): self
    {
        return new self(
            $dimensions
        );
    }

    public function addPiece(Piece $piece): self
    {
        if ($this->isComplete) {
            throw new \LogicException('Cannot add a new piece to a complete table.');
        }

        return new self(
            $this->dimensions,
            ...[...$this->pieces, $piece],
        );
    }

    public function getTable(): Table
    {
        if (!$this->isComplete) {
            throw new \LogicException('Cannot retrieve table, it is not yet complete.');
        }

        return new Table($this->dimensions, ...$this->pieces);
    }
}
