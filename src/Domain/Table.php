<?php

declare(strict_types=1);

namespace LVC\MinisTables\Domain;

final class Table
{
    public const PIECES_COL_SEPARATOR = '!';
    public const PIECES_ROW_SEPARATOR = PHP_EOL;

    /** @var array<int, array<int, Piece>> */
    private readonly array $pieces;
    public readonly string $uniqueDescription;

    public function __construct(
        public readonly TableDimensions $dimensions,
        Piece ...$pieces
    ) {
        if (($numberOfPieces = $dimensions->size) !== count($pieces)) {
            throw new \InvalidArgumentException('For a '.$dimensions->width.'*'.$dimensions->length.' table, you need exactly '.$numberOfPieces.' pieces.');
        }

        $assembledPieces = [];
        for ($y = 0; $y < $this->dimensions->length; $y++) {
            for ($x = 0; $x < $this->dimensions->width; $x++) {
                $piece = array_shift($pieces);
                if ($piece === null) {
                    throw new \InvalidArgumentException('Missing piece at position '.$x.','.$y.'.');
                }

                $assembledPieces[$y][$x] = $piece;
            }
        }

        $this->pieces = $assembledPieces;
        $this->uniqueDescription = self::calculateUniqueDescription($assembledPieces);
    }

    public function getPiece(int $x, int $y): Piece
    {
        if ($x >= $this->dimensions->width || $y >= $this->dimensions->length) {
            throw new \InvalidArgumentException('Cannot ask for a piece out of the table.');
        }

        return $this->pieces[$y][$x];
    }

    /**
     * @return array<Piece>
     */
    public function getAllPieces(): array
    {
        return array_merge(...$this->pieces);
    }

    /**
     * Calculate the description for the arranged pieces,
     *  and the description for all the table variations,
     *  keeping only one to be unique.
     * This will identify identical or rotated tables as being the same.
     *
     * @param array<int, array<int, Piece>> $pieces
     */
    private static function calculateUniqueDescription(array $pieces): string
    {
        $allDescriptions = [
            self::describePieces($pieces),
            self::describePieces($rotated90 = self::rotatePieces($pieces)),
            self::describePieces($rotated180 = self::rotatePieces($rotated90)),
            self::describePieces($rotated270 = self::rotatePieces($rotated180)),
        ];

        sort($allDescriptions);

        return reset($allDescriptions);
    }

    /**
     * @param array<int, array<Piece>> $pieces
     */
    private static function describePieces(array $pieces): string
    {
        return implode(
            self::PIECES_ROW_SEPARATOR,
            array_map(
                fn (array $rowPieces) => implode(
                    self::PIECES_COL_SEPARATOR,
                    array_map(
                        fn (Piece $piece) => $piece->fullDescription,
                        $rowPieces,
                    ),
                ),
                $pieces,
            ),
        );
    }

    /**
     * @param array<int, array<Piece>> $pieces
     *
     * @return array<int, array<Piece>>
     */
    private static function rotatePieces(array $pieces): array
    {
        // To rotate -90° a matrix, you need to reverse its rows, then transpose it.
        $reversedRowPieces = array_map('array_reverse', $pieces);

        // Transposition is done with the array_map trick of a null callback.
        if (count($reversedRowPieces) === 1) {
            // Exception: array_map with unpacking won't work with a 1 column table.
            $transposedPieces = array_map(fn (Piece $piece): array => [$piece], reset($reversedRowPieces));
        } else {
            $transposedPieces = array_map(null, ...$reversedRowPieces);
        }

        // To achieve the rotation, all pieces must rotate to the left
        $rotatedPieces = array_map(
            fn (array $rowPieces) => array_map(
                fn (Piece $piece): Piece => $piece->rotateLeft(),
                $rowPieces
            ),
            $transposedPieces
        );

        return $rotatedPieces;
    }
}
