<?php

declare(strict_types=1);

namespace LVC\MinisTables\Domain;

final class TableDimensions
{
    public readonly int $size;
    private self $cachedRotation;

    public function __construct(
        public readonly int $width,
        public readonly int $length,
    ) {
        if ($width < 1 || $length < 1) {
            throw new \InvalidArgumentException('A table needs a minimum length and width of 1 by 1.');
        }

        $this->size = $this->width * $this->length;
    }

    public function rotate(): self
    {
        if (!isset($this->cachedRotation)) {
            $this->buildCachedRotation();
        }

        return $this->cachedRotation;
    }

    private function buildCachedRotation(): void
    {
        if ($this->length === $this->width) {
            $this->cachedRotation = $this;

            return;
        }

        $this->cachedRotation = new self($this->length, $this->width);
        $this->cachedRotation->cachedRotation = $this;
    }
}
