<?php

declare(strict_types=1);

namespace LVC\MinisTables\Domain;

final class GroundSection
{
    private const HEIGHT_TEXTURE_SEPARATOR = ':';

    public readonly string $description;

    public function __construct(
        public readonly string $texture,
        public readonly int $height,
    ) {
        $this->description = $this->height.self::HEIGHT_TEXTURE_SEPARATOR.$this->texture;
    }
}
