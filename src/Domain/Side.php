<?php

declare(strict_types=1);

namespace LVC\MinisTables\Domain;

final class Side
{
    private const GROUND_SECTIONS_SEPARATOR = ';';

    /** @var array<GroundSection> */
    public readonly array $groundSectionsFromLeftToRight;
    public readonly string $descriptionFromLeftToRight;
    public readonly string $descriptionFromRightToLeft;

    public function __construct(GroundSection ...$groundSectionFromLeftToRight)
    {
        $this->groundSectionsFromLeftToRight = $groundSectionFromLeftToRight;
        $this->descriptionFromLeftToRight = implode(
            self::GROUND_SECTIONS_SEPARATOR,
            array_map(fn (GroundSection $groundSection): string => $groundSection->description, $this->groundSectionsFromLeftToRight)
        );
        $this->descriptionFromRightToLeft = implode(
            self::GROUND_SECTIONS_SEPARATOR,
            array_map(fn (GroundSection $groundSection): string => $groundSection->description, array_reverse($this->groundSectionsFromLeftToRight))
        );
    }
}
