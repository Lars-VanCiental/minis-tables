<?php

declare(strict_types=1);

namespace LVC\MinisTables\Application;

use LVC\MinisTables\Domain\Table;

interface TablesRepository
{
    public function saveTables(TableBuildingInstructions $tableBuildingInstructions, Table ...$tables): void;

    /**
     * @return ?array<int, Table>
     */
    public function getTables(TableBuildingInstructions $tableBuildingInstructions): ?array;
}
