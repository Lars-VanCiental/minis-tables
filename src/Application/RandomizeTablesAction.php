<?php

declare(strict_types=1);

namespace LVC\MinisTables\Application;

use LVC\MinisTables\Domain\Piece;
use LVC\MinisTables\Domain\Table;

final class RandomizeTablesAction
{
    public function __construct(
        private readonly TablesRepository $tablesRepository,
    ) {
    }

    /**
     * @return array<int, Table>
     */
    public function __invoke(
        TableBuildingInstructions $tableBuildingInstructions,
        int $numberOfTables
    ): array {
        if ($numberOfTables < 1) {
            throw new \RuntimeException('Number of tables must be a positive integer.');
        }

        $tables = $this->tablesRepository->getTables($tableBuildingInstructions);
        if ($tables === null) {
            throw new UndiscoveredTables('Tables needs to be discovered before being randomized.');
        }

        $splitTables = self::splitTables(
            $tableBuildingInstructions->pieces,
            ...$tables
        );

        $eligibleTablesGroup = array_filter(
            $splitTables,
            fn (array $tablesGroup): bool => count($tablesGroup) >= $numberOfTables
        );

        if (empty($eligibleTablesGroup)) {
            throw new NotEnoughTables(
                'Not enough tables with different pieces to select '.$numberOfTables.' on them.'
            );
        }

        $selectedGroup = $eligibleTablesGroup[array_rand($eligibleTablesGroup)];
        /** @var int|array<int> $selectedTableKeys */
        $selectedTableKeys = array_rand($selectedGroup, $numberOfTables);
        if (is_int($selectedTableKeys)) {
            $selectedTableKeys = [$selectedTableKeys];
        }

        return array_values(
            array_intersect_key(
                $selectedGroup,
                array_flip($selectedTableKeys),
            )
        );
    }

    /**
     * @param array<Piece> $availablePieces
     *
     * @return array<int, array<int, Table>>
     */
    private static function splitTables(array $availablePieces, Table ...$tables): array
    {
        $tablesGroups = [];
        foreach ($tables as $table) {
            $tablePieces = $table->getAllPieces();
            $newAvailablePiecesCounts = self::isTablePossibleTaRace($availablePieces, $tablePieces);
            if ($newAvailablePiecesCounts === null) {
                continue;
            }

            $otherTables = array_filter($tables, fn (Table $otherTable): bool => $otherTable !== $table);
            if (count($otherTables) === 0 || count($newAvailablePiecesCounts) === 0) {
                $tablesGroups[] = [$table];
            }

            foreach (self::splitTables($newAvailablePiecesCounts, ...$otherTables) as $subTablesGroup) {
                $tablesGroups[] = [$table, ...$subTablesGroup];
            }
        }

        return $tablesGroups;
    }

    /**
     * @param array<Piece> $availablePieces
     * @param array<Piece> $tablePieces
     *
     * @return ?array<Piece>
     */
    private static function isTablePossibleTaRace(array $availablePieces, array $tablePieces): ?array
    {
        foreach ($tablePieces as $tablePiece) {
            foreach ($availablePieces as $index => $availablePiece) {
                foreach ($availablePiece->getAllVariations() as $availablePieceVariation) {
                    if ($tablePiece->fullDescription === $availablePieceVariation->fullDescription) {
                        unset($availablePieces[$index]);
                        continue 3;
                    }
                }
            }

            return null;
        }

        return $availablePieces;
    }
}
