<?php

declare(strict_types=1);

namespace LVC\MinisTables\Application;

use LVC\MinisTables\Domain\Table;
use LVC\MinisTables\Domain\Table\Builder\Builder;
use LVC\MinisTables\Domain\Table\Builder\PiecesSupply;

final class DiscoverTablesAction
{
    public function __construct(
        private readonly TablesRepository $tablesRepository,
    ) {
    }

    /**
     * @return array<int, Table>
     */
    public function __invoke(
        TableBuildingInstructions $tableBuildingInstructions
    ): array {
        $piecesSupply = new PiecesSupply(...$tableBuildingInstructions->pieces);
        $tableBuilder = new Builder($tableBuildingInstructions->pieceValidator);
        $tables = $tableBuilder->createAllTables($piecesSupply, $tableBuildingInstructions->dimensions);

        $this->tablesRepository->saveTables($tableBuildingInstructions, ...$tables);

        return $tables;
    }
}
