<?php

declare(strict_types=1);

namespace LVC\MinisTables\Application;

class UndiscoveredTables extends \RuntimeException
{
}
