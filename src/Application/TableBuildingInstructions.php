<?php

declare(strict_types=1);

namespace LVC\MinisTables\Application;

use LVC\MinisTables\Domain\Piece;
use LVC\MinisTables\Domain\Table\Builder\PieceValidator;
use LVC\MinisTables\Domain\TableDimensions;

final class TableBuildingInstructions
{
    private const DIMENSIONS_OPENING_SEPARATOR = '[';
    private const DIMENSIONS_SEPARATOR = 'x';
    private const DIMENSIONS_CLOSING_SEPARATOR = ']';
    private const PIECES_SEPARATOR = '.';

    /** @var array<Piece> */
    public readonly array $pieces;
    public readonly string $description;

    public function __construct(
        public readonly TableDimensions $dimensions,
        public readonly PieceValidator $pieceValidator,
        Piece ...$pieces
    ) {
        $this->pieces = $pieces;
        $this->description = get_class($this->pieceValidator)
            .self::DIMENSIONS_OPENING_SEPARATOR
            .$this->dimensions->width
            .self::DIMENSIONS_SEPARATOR
            .$this->dimensions->length
            .self::DIMENSIONS_CLOSING_SEPARATOR
            .implode(
                self::PIECES_SEPARATOR,
                array_map(
                    fn (Piece $piece): string => $piece->fullDescription,
                    $this->pieces
                )
            );
    }
}
