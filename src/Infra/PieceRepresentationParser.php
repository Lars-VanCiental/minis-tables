<?php

declare(strict_types=1);

namespace LVC\MinisTables\Infra;

final class PieceRepresentationParser
{
    private const GROUND_SECTIONS_SEPARATOR = ';';
    private const HEIGHT_TEXTURE_SEPARATOR = ':';
    private const NAME_OPENING_SEPARATOR = '<';
    private const NAME_CLOSING_SEPARATOR = '>';
    private const SIDES_SEPARATOR = '^';

    public readonly bool $isValid;
    public readonly string $name;
    /** @var array<int, array{height: int, texture: GroundTexture}> */
    public readonly array $topSide;
    /** @var array<int, array{height: int, texture: GroundTexture}> */
    public readonly array $rightSide;
    /** @var array<int, array{height: int, texture: GroundTexture}> */
    public readonly array $bottomSide;
    /** @var array<int, array{height: int, texture: GroundTexture}> */
    public readonly array $leftSide;

    public function __construct(
        public readonly string $pieceRepresentation
    ) {
        $pattern = '#^'.self::getPiecePattern().'$#';
        $matches = [];

        $this->isValid = preg_match($pattern, $this->pieceRepresentation, $matches) === 1;
        if ($this->isValid === true) {
            $this->name = $matches['name'];
            $this->topSide = self::parseSide($matches['topSide']);
            $this->rightSide = self::parseSide($matches['rightSide']);
            $this->bottomSide = self::parseSide($matches['bottomSide']);
            $this->leftSide = self::parseSide($matches['leftSide']);
        }
    }

    public static function getPiecePattern(): string
    {
        $texturesMatcher = implode(
            '|',
            array_map(fn (GroundTexture $groundTexture): string => $groundTexture->value, GroundTexture::cases())
        );
        $sectionPattern = '\d+'.self::HEIGHT_TEXTURE_SEPARATOR.'(?:'.$texturesMatcher.')';
        $sidePattern = $sectionPattern.'(?:'.self::GROUND_SECTIONS_SEPARATOR.$sectionPattern.')*';

        return '(?<topSide>'.$sidePattern.')'
            .'\\'.self::SIDES_SEPARATOR
            .'(?<rightSide>'.$sidePattern.')'
            .'\\'.self::SIDES_SEPARATOR
            .'(?<bottomSide>'.$sidePattern.')'
            .'\\'.self::SIDES_SEPARATOR
            .'(?<leftSide>'.$sidePattern.')'
            .self::NAME_OPENING_SEPARATOR.'(?<name>\w+)'.self::NAME_CLOSING_SEPARATOR;
    }

    /**
     * @return array<int, array{height: int, texture: GroundTexture}>
     */
    private static function parseSide(string $string): array
    {
        // A piece is described from top left in clockwise order,
        //  meaning each side is described from right to left when facing it.
        return array_reverse(
            array_map(
                function (string $sectionRepresentation): array {
                    [$height, $texture] = explode(
                        self::HEIGHT_TEXTURE_SEPARATOR,
                        $sectionRepresentation
                    );

                    return ['height' => (int) $height, 'texture' => GroundTexture::from($texture)];
                },
                explode(self::GROUND_SECTIONS_SEPARATOR, $string)
            ),
        );
    }
}
