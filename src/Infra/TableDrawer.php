<?php

declare(strict_types=1);

namespace LVC\MinisTables\Infra;

use LVC\MinisTables\Domain\Table;

interface TableDrawer
{
    public function draw(Table ...$tables): void;
}
