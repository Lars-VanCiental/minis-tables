<?php

declare(strict_types=1);

namespace LVC\MinisTables\Infra\Command;

use LVC\MinisTables\Domain\Piece;
use LVC\MinisTables\Domain\Table\Builder\PieceValidator;
use LVC\MinisTables\Domain\Table\Builder\PieceValidator\ValidatePieceByFullSideMatching;
use LVC\MinisTables\Domain\TableDimensions;
use LVC\MinisTables\Infra\Drawer\ConsoleDrawer;
use LVC\MinisTables\Infra\PieceFactory;
use LVC\MinisTables\Infra\TableDrawer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

abstract class AbstractTableCommand extends Command
{
    protected const ARGUMENT_WIDTH_NAME = 'width';
    protected const ARGUMENT_LENGTH_NAME = 'length';
    protected const ARGUMENT_PIECES_NAME = 'pieces';
    protected const OPTION_PIECES_PER_LINE_NAME = 'pieces-per-line';
    protected const OPTION_PIECES_PER_LINE_DEFAULT = 15;
    private const OPTION_RULES_NAME = 'rules';
    private const OPTION_RULES_VALUE_FULL = 'full';
    private const OPTION_RULES_VALUE_HEIGHT = 'height';
    private const OPTION_RULES_VALUE_TEXTURE = 'texture';

    protected TableDimensions $dimensions;
    /** @var array<Piece> */
    protected array $pieces;
    protected PieceValidator $pieceValidator;
    protected SymfonyStyle $io;
    protected TableDrawer $tableDrawer;

    protected function configure(): void
    {
        $this->addArgument(
            self::ARGUMENT_WIDTH_NAME,
            InputArgument::REQUIRED,
            'Width of the desired table'
        );
        $this->addArgument(
            self::ARGUMENT_LENGTH_NAME,
            InputArgument::REQUIRED,
            'Width of the desired table'
        );
        $this->addArgument(
            self::ARGUMENT_PIECES_NAME,
            InputArgument::REQUIRED | InputArgument::IS_ARRAY,
            'A representation of available pieces.'
        );
        $this->addOption(
            self::OPTION_RULES_NAME,
            null,
            InputOption::VALUE_REQUIRED,
            'Rules to be applied to the construction. Available values: none, road, outer, full.',
            self::OPTION_RULES_VALUE_FULL
        );
        $this->addOption(
            self::OPTION_PIECES_PER_LINE_NAME,
            null,
            InputOption::VALUE_REQUIRED,
            'Maximum number of pieces to draw on each console line. A piece is 7 character + a tabulation.',
            self::OPTION_PIECES_PER_LINE_DEFAULT
        );
    }

    protected function init(InputInterface $input, OutputInterface $output): void
    {
        /** @var int $width */
        $width = $input->getArgument(self::ARGUMENT_WIDTH_NAME);
        if (!is_numeric($width)) {
            throw new \InvalidArgumentException('Width must be an int.');
        }
        /** @var int $length */
        $length = $input->getArgument(self::ARGUMENT_LENGTH_NAME);
        if (!is_numeric($length)) {
            throw new \InvalidArgumentException('Length must be an int.');
        }
        $this->dimensions = new TableDimensions((int) $width, (int) $length);

        $piecesRepresentation = $input->getArgument(self::ARGUMENT_PIECES_NAME);
        if (!is_array($piecesRepresentation)) {
            throw new \InvalidArgumentException('Pieces representation must be an array.');
        }
        $this->pieces = array_map(
            fn (string $pieceRepresentation): Piece => PieceFactory::createPiece($pieceRepresentation),
            $piecesRepresentation,
        );

        $this->pieceValidator = match ($input->getOption(self::OPTION_RULES_NAME)) {
            self::OPTION_RULES_VALUE_FULL => new ValidatePieceByFullSideMatching(),
            self::OPTION_RULES_VALUE_HEIGHT => new PieceValidator\ValidatePieceByHeightMatching(),
            self::OPTION_RULES_VALUE_TEXTURE => new PieceValidator\ValidatePieceByTextureMatching(),
            default => throw new \InvalidArgumentException('Unknown value for option "'.self::OPTION_RULES_NAME.'".'),
        };

        /** @var int $maximumPiecesPerLine */
        $maximumPiecesPerLine = $input->getOption(self::OPTION_PIECES_PER_LINE_NAME);
        if (!is_numeric($maximumPiecesPerLine)) {
            throw new \InvalidArgumentException('Pieces per line must be an int.');
        }

        $this->io = new SymfonyStyle($input, $output);

        $this->tableDrawer = new ConsoleDrawer(
            $this->io,
            (int) $maximumPiecesPerLine
        );
    }
}
