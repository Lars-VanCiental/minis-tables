<?php

declare(strict_types=1);

namespace LVC\MinisTables\Infra\Command;

use LVC\MinisTables\Application\DiscoverTablesAction;
use LVC\MinisTables\Application\TableBuildingInstructions;
use LVC\MinisTables\Application\TablesRepository;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

final class TablePresenterCommand extends AbstractTableCommand
{
    private const OPTION_RULES_NAME = 'rules';
    private const OPTION_RULES_VALUE_FULL = 'full';

    public function __construct(
        private readonly TablesRepository $tablesRepository
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        parent::configure();

        $this->setName('table:present');
        $this->addOption(self::OPTION_RULES_NAME, null, InputOption::VALUE_REQUIRED, 'Rules to be applied to the construction. Available values: none, road, outer, full.', self::OPTION_RULES_VALUE_FULL);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->init($input, $output);

        $action = new DiscoverTablesAction($this->tablesRepository);
        $tables = $action(new TableBuildingInstructions($this->dimensions, $this->pieceValidator, ...$this->pieces));

        if (empty($tables)) {
            $this->io->warning('Could not build any table :(');

            return self::SUCCESS;
        }

        $this->io->success('Found '.count($tables).' tables!');

        $this->tableDrawer->draw(...$tables);

        return self::SUCCESS;
    }
}
