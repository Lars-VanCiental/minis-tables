<?php

declare(strict_types=1);

namespace LVC\MinisTables\Infra\Command;

use LVC\MinisTables\Application\DiscoverTablesAction;
use LVC\MinisTables\Application\NotEnoughTables;
use LVC\MinisTables\Application\RandomizeTablesAction;
use LVC\MinisTables\Application\TableBuildingInstructions;
use LVC\MinisTables\Application\TablesRepository;
use LVC\MinisTables\Application\UndiscoveredTables;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

final class TableRandomizerCommand extends AbstractTableCommand
{
    public const OPTION_NUMBER_OF_TABLES = 'number-of-tables';

    public function __construct(
        private readonly TablesRepository $tablesRepository
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        parent::configure();

        $this->setName('table:randomize');
        $this->addOption(self::OPTION_NUMBER_OF_TABLES, null, InputOption::VALUE_REQUIRED, 'Desired number of tables', 1);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->init($input, $output);

        $numberOfTables = $input->getOption(self::OPTION_NUMBER_OF_TABLES);
        if (!is_numeric($numberOfTables)) {
            $this->io->error('The number-of-tables option must be a positive integer.');

            return self::FAILURE;
        }
        $numberOfTables = (int) $numberOfTables;
        if ($numberOfTables < 1) {
            $this->io->error('The number-of-tables option must be a positive integer.');

            return self::FAILURE;
        }

        $action = new RandomizeTablesAction($this->tablesRepository);
        $tableBuildingInstructions = new TableBuildingInstructions($this->dimensions, $this->pieceValidator, ...$this->pieces);

        try {
            $tables = $action($tableBuildingInstructions, $numberOfTables);
        } catch (UndiscoveredTables $undiscoveredTables) {
            $discoverAction = new DiscoverTablesAction($this->tablesRepository);
            $discoverAction($tableBuildingInstructions);

            $tables = $action($tableBuildingInstructions, $numberOfTables);
        } catch (NotEnoughTables $notEnoughTables) {
            $this->io->error('Could not find enough table with different pieces to generate '.$numberOfTables.' tables.');

            return self::FAILURE;
        }

        $this->io->success('Selected '.count($tables).' tables!');

        $this->tableDrawer->draw(...$tables);

        return self::SUCCESS;
    }
}
