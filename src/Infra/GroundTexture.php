<?php

declare(strict_types=1);

namespace LVC\MinisTables\Infra;

enum GroundTexture: string
{
    case ASPHALT = 'asphalt';
    case CONCRETE = 'concrete';
    case COBBLES = 'cobbles';
    case DIRT = 'dirt';
    case GRASS = 'grass';
    case PATH = 'path';
    case ROCK = 'rock';
    case SAND = 'sand';
    case STONE = 'stone';
    case WATER = 'water';
}
