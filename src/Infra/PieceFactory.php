<?php

declare(strict_types=1);

namespace LVC\MinisTables\Infra;

use LVC\MinisTables\Domain\GroundSection;
use LVC\MinisTables\Domain\Piece;
use LVC\MinisTables\Domain\Side;

final class PieceFactory
{
    public static function createPiece(string $pieceTopClockwiseRepresentation): Piece
    {
        $parser = new PieceRepresentationParser($pieceTopClockwiseRepresentation);
        if ($parser->isValid === false) {
            throw new \RuntimeException(
                'Invalid piece representation given ('.$pieceTopClockwiseRepresentation.'), it must matches "'.PieceRepresentationParser::getPiecePattern().'".'
            );
        }

        return new Piece(
            $parser->name,
            self::createSide($parser->topSide),
            self::createSide($parser->rightSide),
            self::createSide($parser->bottomSide),
            self::createSide($parser->leftSide),
        );
    }

    /**
     * @param array<int, array{height: int, texture: GroundTexture}> $parsedSide
     */
    private static function createSide(array $parsedSide): Side
    {
        return new Side(
            ...array_map(
                fn (array $parsedSection): GroundSection => new GroundSection($parsedSection['texture']->value, $parsedSection['height']),
                $parsedSide
            )
        );
    }
}
