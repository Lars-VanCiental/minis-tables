<?php

declare(strict_types=1);

namespace LVC\MinisTables\Infra;

use LVC\MinisTables\Application\TableBuildingInstructions;
use LVC\MinisTables\Application\TablesRepository;
use LVC\MinisTables\Domain\Table;

class InMemoryTablesRepository implements TablesRepository
{
    /** @var array<string, array<Table>> */
    private array $tablesByInstructions = [];

    public function saveTables(TableBuildingInstructions $tableBuildingInstructions, Table ...$tables): void
    {
        $this->tablesByInstructions[$tableBuildingInstructions->description] = $tables;
    }

    public function getTables(TableBuildingInstructions $tableBuildingInstructions): ?array
    {
        return $this->tablesByInstructions[$tableBuildingInstructions->description] ?? null;
    }
}
