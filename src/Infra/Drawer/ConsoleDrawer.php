<?php

declare(strict_types=1);

namespace LVC\MinisTables\Infra\Drawer;

use LVC\MinisTables\Domain\GroundSection;
use LVC\MinisTables\Domain\Piece;
use LVC\MinisTables\Domain\Side;
use LVC\MinisTables\Domain\Table;
use LVC\MinisTables\Infra\GroundTexture;
use LVC\MinisTables\Infra\TableDrawer;
use Symfony\Component\Console\Style\SymfonyStyle;

final class ConsoleDrawer implements TableDrawer
{
    private const PIECE_SIZE = 5;
    private const EMPTY = ' ';
    private const UPPER_LEFT = '┌';
    private const UPPER_RIGHT = '┐';
    private const LOWER_LEFT = '└';
    private const LOWER_RIGHT = '┘';

    public function __construct(
        private readonly SymfonyStyle $io,
        private readonly int $maximumNumberOfPiecesPerLine
    ) {
    }

    public function draw(Table ...$tables): void
    {
        $tablesBySize = self::splitTablesBySize(...$tables);

        foreach ($tablesBySize as $tablesLength => $tablesByLength) {
            foreach ($tablesByLength as $tablesWidth => $tableByWidth) {
                $tablesToDraw = [];
                $numberOfPiecesToDraw = 0;

                foreach ($tableByWidth as $tableToDraw) {
                    $numberOfPiecesToDraw += $tablesWidth + 1;
                    $tablesToDraw[] = $tableToDraw;

                    if ($numberOfPiecesToDraw > $this->maximumNumberOfPiecesPerLine) {
                        $this->drawTables($tablesLength, ...$tablesToDraw);

                        $tablesToDraw = [];
                        $numberOfPiecesToDraw = 0;
                        $this->io->newLine(2);
                    }
                }

                if (!empty($tablesToDraw)) {
                    $this->drawTables($tablesLength, ...$tablesToDraw);
                }
            }
        }
    }

    /**
     * @return array<int,array<int,array<Table>>>
     */
    private static function splitTablesBySize(Table ...$tables): array
    {
        $tablesBySize = [];

        foreach ($tables as $table) {
            $tablesBySize[$table->dimensions->length][$table->dimensions->width][] = $table;
        }

        return $tablesBySize;
    }

    private function drawTables(int $tablesLength, Table ...$tables): void
    {
        for ($y = 0; $y < $tablesLength; $y++) {
            foreach ($tables as $table) {
                $this->drawTableTopHorizontalLine($y, $table);
                $this->drawTableSeparator();
            }
            $this->drawEndOfLine();

            for ($line = 0; $line < self::PIECE_SIZE; $line++) {
                foreach ($tables as $table) {
                    $this->drawTableVerticalLine($y, $table, $line + 1);
                    $this->drawTableSeparator();
                }
                $this->drawEndOfLine();
            }

            foreach ($tables as $table) {
                $this->drawTableBottomHorizontalLine($y, $table);
                $this->drawTableSeparator();
            }
            $this->drawEndOfLine();
        }
    }

    private function drawTableTopHorizontalLine(int $y, Table $table): void
    {
        for ($x = 0; $x < $table->dimensions->width; $x++) {
            $piece = $table->getPiece($x, $y);
            $this->drawPieceHorizontalTopLine($piece->topSide);
        }
    }

    private function drawPieceHorizontalTopLine(Side $side): void
    {
        $this->io->write(self::UPPER_LEFT);
        for ($position = 0; $position < self::PIECE_SIZE; $position++) {
            $this->io->write(self::getGroundRepresentation(self::getPieceGroundSection($position + 1, $side)));
        }
        $this->io->write(self::UPPER_RIGHT);
    }

    private function drawTableBottomHorizontalLine(int $y, Table $table): void
    {
        for ($x = 0; $x < $table->dimensions->width; $x++) {
            $piece = $table->getPiece($x, $y);
            $this->drawPieceHorizontalBottomLine($piece->bottomSide);
        }
    }

    private function drawPieceHorizontalBottomLine(Side $side): void
    {
        $this->io->write(self::LOWER_LEFT);
        for ($position = self::PIECE_SIZE - 1; $position >= 0; $position--) {
            $this->io->write(self::getGroundRepresentation(self::getPieceGroundSection($position + 1, $side)));
        }
        $this->io->write(self::LOWER_RIGHT);
    }

    private function drawTableVerticalLine(int $y, Table $table, int $line): void
    {
        for ($x = 0; $x < $table->dimensions->width; $x++) {
            $this->drawPieceLine($table->getPiece($x, $y), $line);
        }
    }

    private function drawPieceLine(Piece $piece, int $line): void
    {
        $pieceName = $piece->name;
        $maxPieceSize = pow(self::PIECE_SIZE, 2);
        if (strlen($pieceName) > $maxPieceSize) {
            $pieceName = substr_replace($pieceName, '..', $maxPieceSize - 2);
        }
        $pieceName = str_pad($pieceName, $maxPieceSize, self::EMPTY, STR_PAD_BOTH);
        $pieceNameCharacters = str_split(str_split($pieceName, self::PIECE_SIZE)[$line - 1]);

        $this->io->write(self::getGroundRepresentation(self::getPieceGroundSection(self::PIECE_SIZE - $line + 1, $piece->leftSide)));
        for ($position = 0; $position < self::PIECE_SIZE; $position++) {
            $this->io->write($pieceNameCharacters[$position]);
        }
        $this->io->write(self::getGroundRepresentation(self::getPieceGroundSection($line, $piece->rightSide)));
    }

    private function drawEndOfLine(): void
    {
        $this->io->writeln('');
    }

    private function drawTableSeparator(): void
    {
        $this->io->write("\t\t");
    }

    private static function getPieceGroundSection(int $position, Side $side): ?GroundSection
    {
        $numberOfSections = count($side->groundSectionsFromLeftToRight);
        if ($position === 1) {
            return $side->groundSectionsFromLeftToRight[$numberOfSections - 1];
        }
        if ($position === 2) {
            if ($numberOfSections > 3) {
                return $side->groundSectionsFromLeftToRight[$numberOfSections - 2];
            }

            return $side->groundSectionsFromLeftToRight[$numberOfSections - 1];
        }
        if ($position === 3) {
            if ($numberOfSections % 2 === 1) {
                return $side->groundSectionsFromLeftToRight[($numberOfSections - 1) / 2];
            }

            return null;
        }
        if ($position === 4) {
            if ($numberOfSections > 3) {
                return $side->groundSectionsFromLeftToRight[1];
            }

            return $side->groundSectionsFromLeftToRight[0];
        }
        if ($position === 5) {
            return $side->groundSectionsFromLeftToRight[0];
        }

        throw new \InvalidArgumentException('Can only work with position from 1 to 5.');
    }

    private static function getGroundRepresentation(?GroundSection $groundSection): string
    {
        if ($groundSection === null) {
            return self::EMPTY;
        }

        return match ($groundSection->texture) {
            GroundTexture::ASPHALT->value => '<fg=gray>%</>',
            GroundTexture::COBBLES->value => '<fg=gray>ø</>',
            GroundTexture::CONCRETE->value => '<fg=gray>█</>',
            GroundTexture::DIRT->value => '<fg=yellow>_</>',
            GroundTexture::GRASS->value => '<fg=green>"</>',
            GroundTexture::PATH->value => '<fg=yellow>-</>',
            GroundTexture::ROCK->value => '<fg=gray>/</>',
            GroundTexture::SAND->value => '<fg=yellow>.</>',
            GroundTexture::STONE->value => '<fg=gray>O</>',
            GroundTexture::WATER->value => '<fg=blue>~</>',
            default => '?'
        };
    }
}
